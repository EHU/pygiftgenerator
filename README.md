`pygiftgenerator` is a module designed to prepare numerical 
and multi-choice numerical questions for Moodle using python.

Questions are written to output files using the GIFT 
format so that they easily can be imported from Moodle.

The copyright of this software corresponds to its authors, 
Jon Saenz, Idoia G. de Gurtubay, Zunbeltz Izaola, 
employees at the University of the Basque Country, 
Universidad del Pais Vasco, Euskal Herriko Unibertsitatea UPV/EHU.

pyGIFTGenerator code released in 2020 under GPL 3.
For details, see 
https://www.gnu.org/licenses/gpl.html
and 
https://www.gnu.org/licenses/gpl.txt
