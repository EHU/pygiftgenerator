import pygiftgenerator as pyg
import math

cat="myQ/Kinematics/AccelMC"
questionFMT="A body is thrown upwards with "
questionFMT+="vertical velocity "
questionFMT+="v<sub>0</sub>\=%3.1f m s<sup>-1</sup> "
questionFMT+="in a planet with time-varying "
questionFMT+="acceleration given by a\=&minus;2t"
questionFMT+="<sup>2</sup> m s<sup>-2</sup>. Which is its "
questionFMT+="vertical displacement at t=%3.1f s?"
v0s=[20.,30.,40.]
ts=[3.,4.]
ps=pyg.RandomInputParameters()
ps.AppendParameters(v0s)
ps.AppendParameters(ts)
fmt="&Delta;y\= %7.2f m"
wei=[100,-5,-5]
skrep=True
for iQ in range(5):
    qname="TVA_%2.2d_MC"%(iQ,)
    v0,t=ps.GetParameterList()
    qtext=questionFMT%(v0,t)
    right=v0*t-math.pow(t,4.)/6.
    wrong1=v0*t-t*t*t
    wrong2=v0*t+math.pow(t,4.)/6.
    ans=[fmt%(right,),fmt%(wrong1,),
            fmt%(wrong2,)]
    pyg.BuildMulChoiceQuestion(cat,qname,qtext,
            ans,wei,skrep)
