import pygiftgenerator as pyg

category="myQ/DC_Circuit/NumQ"
questionFMT="[html]An electromotive source with "
questionFMT+="potential difference &epsilon;\=%.1f V "
questionFMT+="is connected to two resistors "
questionFMT+="R<sub>1</sub>\=%.1f &Omega; and R<sub>2</sub>"
questionFMT+="\=%.1f &Omega; in parallel. Which is the "
questionFMT+="power dissipated by R<sub>2</sub>?"
units="W"
Vs=[12.,15.]
R1s=[3300.,20000.,12000.]
R2s=[500.,330.]
ps=pyg.RandomInputParameters()
ps.AppendParameters(Vs)
ps.AppendParameters(R1s)
ps.AppendParameters(R2s)
tolerance=0.05
for iQ in range(3):
    qname="Q_DC_%2.2d"%(iQ,)
    # Extract parameters from list
    V,R1,R2=ps.GetParameterList()
    qtext=questionFMT%(V,R1,R2)
    I2=float(V)/float(R2)
    P2=I2*V
    pyg.BuildNumQuestion(category, qname, qtext,
            units, P2,tolerance)
