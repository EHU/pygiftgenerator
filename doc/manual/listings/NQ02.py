import pygiftgenerator as pyg
import random

class GET_P2(pyg.GIFTNumQ):
    # Provide a constructor with data
    # that are passed
    # to the default constructor and 
    # are particular for this case
    def __init__(self,cat,qN,qT,V,R1,R2):
        pyg.GIFTNumQ.__init__(self,cat,qN,qT,"W")
        self.V=V
        self.R1=R1
        self.R2=R2
    def Correct(self):
        self.I2=self.V/self.R2
        return self.I2*self.V

random.seed()
category="myQ/DC_Circuit/NumQ"
questionFMT="[html]An electromotive source with "
questionFMT+="potential difference &epsilon;\=%.1f V "
questionFMT+="is connected to two resistors "
questionFMT+="R<sub>1</sub>\=%.1f &Omega; and R<sub>2</sub>"
questionFMT+="\=%.1f &Omega; in parallel. Which is the "
questionFMT+="power dissipated by R<sub>2</sub>?"
units="W"
for iQ in range(3):
    qname="Q_DC_%2.2d"%(iQ,)
    # Extract parameters from list
    V=random.gauss(12.,2)
    R1=random.gauss(12500.,1)
    R2=random.gauss(330.,3)
    qtext=questionFMT%(V,R1,R2)
    q=GET_P2(category,qname,qtext,V,R1,R2)
    q.DumpGift()
