# Random Input Selectors, from lists
# T and pressure in valid ranges
Ps=[500,550]
Ts=[-30,-28,-25,-20,-15]
# New input parameters
R1=RandomInputParameters()
R1.AppendParameters(Ps)
R1.AppendParameters(Ts)
########################
Ps=[600]
Ts=[-25,-22,-20,-17,-15,-12,-10]
# New input parameters
R2=RandomInputParameters()
R2.AppendParameters(Ps)
R2.AppendParameters(Ts)
Ps=[700]
Ts=[-15,-12,-10,-8,-5,-2,0]
# New input parameters
R3=RandomInputParameters()
R3.AppendParameters(Ps)
R3.AppendParameters(Ts)
Ps=[850]
Ts=[-10,-7,-5,0,3,5,7,10]
# New input parameters
R4=RandomInputParameters()
R4.AppendParameters(Ps)
R4.AppendParameters(Ts)
# This is now a set of consistent lists
# for reasonable temperatures and pressures
randomizers=[R1,R2,R3,R4]
for iQ in range(nQuestions):
    # Select either R1 ... R4 (typical 500, 850 hPa)
    Ri=randomizers[random.randint(0,3)]
    plist=Ri.GetParameterList()
