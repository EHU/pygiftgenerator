# Import the constructor of questions
from pygiftgenerator import GIFTNumQ
# And the randomizers for parameter inputs
from pygiftgenerator import RandomInputParameters
# Calculate the intensity flowing through R2
class GET_IR2(GIFTNumQ):
    # Provide a constructor 
    def __init__(self,rootCat,qName,question,
        units,tol,V1,V2,R3,R4):
        GIFTNumQ.__init__(self,rootCat,qName,
          question,units,tol)
        self.V1,self.V2=(V1,V2)
        self.R3,self.R4=(R3,R4)
        self.R1,self.R2=(10.,12.)
        self.delta=(self.R1+self.R2)*(self.R1+self.R2)
        self.delta=self.delta-(self.R1+self.R2+self.R3)
               *(self.R1+self.R2+self.R4)
        NUM=-(self.V1+self.V2)*self.R3-self.V1*self.R4
        self.I2=NUM/self.delta
    # This method calculates the solution and 
    # musts be provided so that the
    # inherited class works
    def Correct(self):
        return self.I2
