# Import the constructor of questions
from pygiftgenerator import GIFTNumQ
# Velocity of the CM
class GET_CMv(GIFTNumQ):
    def __init__(self,rootCat,qName,question,
        units,tol,E,E0,ofile):
        GIFTNumQ.__init__(self,rootCat,qName,
        question,units,tol,ofileObject=ofile)
        self.E=E
        self.E0=E0
        self.c=299792458.
        self.v=self.c*math.sqrt(
            (self.E-self.E0)/(self.E+self.E0))
    def Correct(self):
        return self.v
# Energy of the system measured from the CM
class GET_CME(GIFTNumQ):
    def __init__(self,rootCat,qName,question,units,
        tol,E,E0,ofile):
        GIFTNumQ.__init__(self,rootCat,qName,
        question,units,tol,ofileObject=ofile)
        self.E=E
        self.E0=E0
        self.Estar=math.sqrt(2*self.E0*(self.E+self.E0))
    def Correct(self):
        return self.Estar
