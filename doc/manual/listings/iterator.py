import pygiftgenerator as pyg
# Parameters are built combining from these lists
Vs=[6,9,12,24,36,50,100,200]
R1s=[1000.,2300.,3300.,5500.,1.0e5,3.3e5,1.0e6]
R2s=[1.0e3,1500,2000,2500,3000,5.0e3,8.8e4]
Cs=[2.5e-6,3.9e-5,3.3e-6,3.9e-6,4.9e-6]
# Random parameters 
randIntParams=pyg.RandomInputParameters()
randIntParams.AppendParameters(Vs)
randIntParams.AppendParameters(R1s)
randIntParams.AppendParameters(R2s)
randIntParams.AppendParameters(Cs)
nQuestions=20
for iq in range(nQuestions):
    category="RCCircuit_Charge"
    plist=randIntParams.GetParameterList()
    V,R1,R2,C=plist
