# question ID
qID=0
# High temperatures
tH=numpy.array([5000.,4000.,3000.,2000.,1000.])
# Radiation in the hundred-nanometer scale
wLS=numpy.array([400.,500.,600.,700.])*1.0e-9
for t in tH:
    for w in wLS:
        for iq in range(nQs):
            qName="blackbody_%4.4d"%(qID,)
            temp=random.gauss(t,100.)
            wl=random.gauss(w/1.0e-6,.005)*1.e-6
            q=BlackBody(qName,temp,wl)
            q.DumpGift()
            qID+=1
