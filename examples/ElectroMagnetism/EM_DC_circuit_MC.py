# EM_DC_circuit_MC.py
# 
# A generator of multiple choice questions regarding a DC circuit
# based on pygiftgenerator.py
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# The copyright of this software corresponds to its authors, 
# Jon Saenz, 
# Idoia G. Gurtubay, 
# Zunbeltz Izaola, 
# employees at the University of the Basque Country, 
# Universidad del Pais Vasco/Euskal Herriko Unibertsitatea (UPV/EHU).
#
# Code released in 2020 under GPL 3.
# For details, see https://www.gnu.org/licenses/gpl.html
# and https://www.gnu.org/licenses/gpl.txt
# 

import math,sys,random

# Import the constructor of questions
from pygiftgenerator import BuildMulChoiceQuestion

# And the randomizers for parameter inputs
from pygiftgenerator import RandomInputParameters

# From double precision answers, built string ones with units
def l2s(answers,units,strFormat="%.4g %s"):
    olist=[]
    for a in answers:
        olist.append(strFormat%(a,units))
    return olist

# This is the common Delta
def Delta(R1,R2,R3,R4):
    return R1*R3+R2*R3+R1*R4+R2*R4+R3*R4

# Correct and wrong solutions for the first sub-example
def AnswersI2(V1,V2,R1,R2,R3,R4):
    delta=Delta(R1,R2,R3,R4)
    # Correct value
    c=(V1*R4+R3*(V1+V2))/delta
    # For the wrong values, change some signs and resistors
    w1=(-V1*R4+R3*(V1+V2))/delta
    w2=(V2*R4+R3*V1)/delta
    w3=((V1+V2)*R4+R3*(V2))/delta
    w4=((V1+V2)*R4-R3*(V1))/delta
    return l2s([c,w1,w2,w3,w4],"A")

# Correct and wrong values for I3, recycled in PR3
def I3values(V1,V2,R1,R2,R3,R4):
    delta=Delta(R1,R2,R3,R4)
    # Correct value
    c=(-V2*R1-V2*R2+R4*V1)/delta
    # Wrong values
    w1=(V2*R1+V2*R2+R4*V1)/delta
    w2=(-V2*R1+V2*R2+R4*V1)/delta
    w3=(V2*R1-V2*R2+R4*V1)/delta
    w4=(-V2*(R1+R2)-(V2+V1)*R4)/delta
    return [c,w1,w2,w3,w4]
    
# Correct and wrong solutions for the first sub-example
def AnswersI3(V1,V2,R1,R2,R3,R4):
    return l2s(I3values(V1,V2,R1,R2,R3,R4),"A")

# Correct and wrong solutions for the first sub-example
def AnswersPR3(V1,V2,R1,R2,R3,R4):
    I3s=I3values(V1,V2,R1,R2,R3,R4)
    P3=[]
    for i3 in I3s:
        P3.append(i3*i3*R3)
    return l2s(P3,"W")

# Usually, you call this 
# random.seed()
# But we need a fixed seed for repeatibility (regression tests)
random.seed(123897977)

    
# Parameters are built from these lists
V1s=[12,15,18,20,24]
V2s=[3,5,8,9]
R3=[10,12,15,18,20,24,25,28,30,35,45]
R4=[ 25, 30,40,50, 80,90]
R1=10.
R2=12.

# Random parameters 
randIntParams=RandomInputParameters()
randIntParams.AppendParameters(V1s)
randIntParams.AppendParameters(V2s)
randIntParams.AppendParameters(R3)
randIntParams.AppendParameters(R4)
####


nQuestions=30
category="pyGIFT/ElectroMagnetism/Circuits/DC/MC"
skipRep=True
for iq in range(nQuestions):
    plist=randIntParams.GetParameterList()
    if len(plist)==0:
        print("// No questions left after ",iq)
        break
    V1=plist[0]
    V2=plist[1]
    R3=plist[2]
    R4=plist[3]
    # Fixed weights
    weights=[100,-5,-5,-5,-5]
    # Do not build the questions for I2 and I3 at the same time with the
    # same parameters
    if 0<=iq<10:
        question="[html]The electromotive forces in the circuit shown in the figure are V<sub>1</sub>\=%.1f V and V<sub>2</sub>\=%.1f V. On the other hand, the resistors are given by R<sub>1</sub>\= 10 &Omega;, R<sub>2</sub>\=12 &Omega;, R<sub>3</sub>\=%.1f &Omega; and R<sub>4</sub>\=%.1f &Omega;. Compute the intensity flowing through resistor R<sub>2</sub> from R<sub>1</sub> towards point a.<p><img src\\=\"http://www.ehu.eus/eolo/pyGIFT/Circuit1.png\" height=\"171\" width=\"193\" ></p>"%(V1,V2,R3,R4)
        qName="C1_GetI2_%3.3d_MC"%(iq,)
        answers=AnswersI2(V1,V2,R1,R2,R3,R4)
    elif 10<=iq<20:
        question="[html]The electromotive forces in the circuit shown in the figure are V<sub>1</sub>\=%.1f V and V<sub>2</sub>\=%.1f V. On the other hand, the resistors are given by R<sub>1</sub>\= 10 &Omega;, R<sub>2</sub>\=12 &Omega;, R<sub>3</sub>\=%.1f &Omega; and R<sub>4</sub>\=%.1f &Omega;. Compute the intensity flowing through R<sub>3</sub> from point a to b.<p><img src\\=\"http://www.ehu.eus/eolo/pyGIFT/Circuit1.png\" height=\"171\" width=\"193\" ></p>"%(V1,V2,R3,R4)
        qName="C1_GetI3_%3.3d_MC"%(iq,)
        answers=AnswersI3(V1,V2,R1,R2,R3,R4)
    else:
        question="[html]The electromotive forces in the circuit shown in the figure are V<sub>1</sub>\=%.1f V and V<sub>2</sub>\=%.1f V. On the other hand, the resistors are given by R<sub>1</sub>\= 10 &Omega;, R<sub>2</sub>\=12 &Omega;, R<sub>3</sub>\=%.1f &Omega; and R<sub>4</sub>\=%.1f &Omega;. Compute the power dissipated by R<sub>3</sub>.<p><img src\\=\"http://www.ehu.eus/eolo/pyGIFT/Circuit1.png\" height=\"171\" width=\"193\" ></p>"%(V1,V2,R3,R4)
        qName="C1_GetPR3_%3.3d_MC"%(iq,)
        answers=AnswersPR3(V1,V2,R1,R2,R3,R4)
    BuildMulChoiceQuestion(category,qName,question,answers,weights,skipRep)

    
