# EM_DC_circuit_NQ.py
# 
# Example of application of pygiftgenerator to the solution of
# different quantities for the case of a DC circuit.
# NQ: Numerical Question
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# The copyright of this software corresponds to its authors, 
# Jon Saenz, 
# Idoia G. Gurtubay, 
# Zunbeltz Izaola, 
# employees at the University of the Basque Country, 
# Universidad del Pais Vasco/Euskal Herriko Unibertsitatea (UPV/EHU).
#
# Code released in 2020 under GPL 3.
# For details, see https://www.gnu.org/licenses/gpl.html
# and https://www.gnu.org/licenses/gpl.txt
# 

import math
import numpy
import random

# Import the constructor of questions
from pygiftgenerator import GIFTNumQ
# And the randomizers for parameter inputs
from pygiftgenerator import RandomInputParameters

# Calculate the intensity flowing through R2
class GET_IR2(GIFTNumQ):
    # Provide a constructor with the needed data (the ones that are passed
    # to the default constructor and teh new ones needed in this case
    def __init__(self,rootCat,qName,question,units,tol,V1,V2,R3,R4):
        GIFTNumQ.__init__(self,rootCat,qName,question,units,tol)
        self.V1=V1
        self.V2=V2
        self.R3=R3
        self.R4=R4
        self.R1=10.
        self.R2=12.
        self.delta=(self.R1+self.R2)*(self.R1+self.R2)
        self.delta=self.delta-(self.R1+self.R2+self.R3)*(self.R1+self.R2+
                                                         self.R4)
        self.I2=(-(self.V1+self.V2)*self.R3-self.V1*self.R4)/self.delta
    # This method calculates the solution and is the expected one
    # that needs being provided by this intance so that the
    # inherited class works
    def Correct(self):
        return self.I2

# Calculate the intensity flowing through R3
class GET_IR3(GIFTNumQ):
    # Provide a constructor with the needed data (the ones that are passed
    # to the default constructor and teh new ones needed in this case
    def __init__(self,rootCat,qName,question,units,tol,V1,V2,R3,R4):
        GIFTNumQ.__init__(self,rootCat,qName,question,units,tol)
        self.V1=V1
        self.V2=V2
        self.R3=R3
        self.R4=R4
        self.R1=10.
        self.R2=12.
        self.delta=(self.R1+self.R2)*(self.R1+self.R2)
        self.delta=self.delta-(self.R1+self.R2+self.R3)*(self.R1+self.R2+
                                                         self.R4)
        self.a11=self.R1+self.R2
        self.a12=self.V1
        self.a21=self.R1+self.R2+self.R4
        self.a22=self.V1+self.V2
        self.I3=(self.a11*self.a22-self.a12*self.a21)/self.delta
    # This method calculates the solution and is the expected one
    # that needs being provided by this intance so that the
    # inherited class works
    def Correct(self):
        return self.I3
    
# Calculate the power disipated by R3
class GET_PR3(GIFTNumQ):
    # Provide a constructor with the needed data (the ones that are passed
    # to the default constructor and teh new ones needed in this case
    def __init__(self,rootCat,qName,question,units,tol,V1,V2,R3,R4):
        GIFTNumQ.__init__(self,rootCat,qName,question,units,tol)
        self.V1=V1
        self.V2=V2
        self.R3=R3
        self.R4=R4
        self.R1=10.
        self.R2=12.
        self.delta=(self.R1+self.R2)*(self.R1+self.R2)
        self.delta=self.delta-(self.R1+self.R2+self.R3)*(self.R1+self.R2+
                                                         self.R4)
        self.a11=self.R1+self.R2
        self.a12=self.V1
        self.a21=self.R1+self.R2+self.R4
        self.a22=self.V1+self.V2
        self.I3=(self.a11*self.a22-self.a12*self.a21)/self.delta
        self.PR3=self.I3*self.I3*self.R3
    # This method calculates the solution and is the expected one
    # that needs being provided by this intance so that the
    # inherited class works
    def Correct(self):
        return self.PR3
    
# Usually, you call this 
# random.seed()
# But we need a fixed seed for repeatibility (regression tests)
random.seed(123897977)

# Parameters are built from these lists
V1s=[12,15,18,20,24]
V2s=[3,5,8,9]
R3=[10,12,15,18,20,24,25,28,30,35,45]
R4=[ 25, 30,40,50, 80,90]

# Random parameters 
randIntParams=RandomInputParameters()
randIntParams.AppendParameters(V1s)
randIntParams.AppendParameters(V2s)
randIntParams.AppendParameters(R3)
randIntParams.AppendParameters(R4)
####

nQuestions=30
category="pyGIFT/ElectroMagnetism/Circuits/DC/NQ"
tol=0.075
for iq in range(nQuestions):
    plist=randIntParams.GetParameterList()
    if len(plist)==0:
        print("// No questions left after ",iq)
        break
    V1=plist[0]
    V2=plist[1]
    R3=plist[2]
    R4=plist[3]
    # Do not build the questions for I2 and I3 at the same time with the
    # same parameters
    if 0<=iq<10:
        question="[html]The electromotive forces in the circuit shown in the figure are V<sub>1</sub>\=%.1f V and V<sub>2</sub>\=%.1f V. On the other hand, the resistors are given by R<sub>1</sub>\= 10 &Omega;, R<sub>2</sub>\=12 &Omega;, R<sub>3</sub>\=%.1f &Omega; and R<sub>4</sub>\=%.1f &Omega;. Compute the intensity flowing through resistor R<sub>2</sub> from R<sub>1</sub> towards point a.<p><img src\\=\"http://www.ehu.eus/eolo/pyGIFT/Circuit1.png\" height=\"171\" width=\"193\" ></p>"%(V1,V2,R3,R4)
        qName="C1_GetI2_%3.3d_NQ"%(iq,)
        q=GET_IR2(category,qName,question,"A",tol,V1,V2,R3,R4)
    elif 10<=iq<20:
        question="[html]The electromotive forces in the circuit shown in the figure are V<sub>1</sub>\=%.1f V and V<sub>2</sub>\=%.1f V. On the other hand, the resistors are given by R<sub>1</sub>\= 10 &Omega;, R<sub>2</sub>\=12 &Omega;, R<sub>3</sub>\=%.1f &Omega; and R<sub>4</sub>\=%.1f &Omega;. Compute the intensity flowing through R<sub>3</sub> from point a to b.<p><img src\\=\"http://www.ehu.eus/eolo/pyGIFT/Circuit1.png\" height=\"171\" width=\"193\" ></p>"%(V1,V2,R3,R4)
        qName="C1_GetI3_%3.3d_NQ"%(iq,)
        q=GET_IR3(category,qName,question,"A",tol,V1,V2,R3,R4)
    else:
        question="[html]The electromotive forces in the circuit shown in the figure are V<sub>1</sub>\=%.1f V and V<sub>2</sub>\=%.1f V. On the other hand, the resistors are given by R<sub>1</sub>\= 10 &Omega;, R<sub>2</sub>\=12 &Omega;, R<sub>3</sub>\=%.1f &Omega; and R<sub>4</sub>\=%.1f &Omega;. Compute the power dissipated by R<sub>3</sub>.<p><img src\\=\"http://www.ehu.eus/eolo/pyGIFT/Circuit1.png\" height=\"171\" width=\"193\" ></p>"%(V1,V2,R3,R4)
        qName="C1_GetPR3_%3.3d_NQ"%(iq,)
        q=GET_PR3(category,qName,question,"W",tol,V1,V2,R3,R4)
    q.DumpGift()

