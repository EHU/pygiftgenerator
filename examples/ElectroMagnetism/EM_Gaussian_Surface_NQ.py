# EM_Gaussian_Surface_NQ.py
# 
# Application of pygiftgenerator to an example involving Gaussian
# surfaces for the determination of electric field and potential 
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# The copyright of this software corresponds to its authors, 
# Jon Saenz, 
# Idoia G. Gurtubay, 
# Zunbeltz Izaola, 
# employees at the University of the Basque Country, 
# Universidad del Pais Vasco/Euskal Herriko Unibertsitatea (UPV/EHU).
#
# Code released in 2020 under GPL 3.
# For details, see https://www.gnu.org/licenses/gpl.html
# and https://www.gnu.org/licenses/gpl.txt
# 

import random
import math
# Import the Fortran-like constructor of questions
from pygiftgenerator import BuildNumQuestion
# And the randomizers for parameter inputs
from pygiftgenerator import RandomInputParameters

# Case A
# Long non-conductor cylinder with charge density
# \rho(r)=\rho_0\left(1-\left(\frac{r}{R}\right)^n\right)
# Potential out
def A_VOut(rho,n,R,r,eps0):
    V=-rho*R*R/eps0*(1./2.-1./(n+2.))*math.log(r/R)
    return V
# potential inside
def A_VIn(rho,n,R,r,eps0):
    V=rho/eps0/4.*(R*R-r*r)-rho/eps0/math.pow(R,n)/math.pow(n+2,2)*(math.pow(R,n+2)-math.pow(r,n+2))
    return V
# Potential everywhere
def A_V(rho,n,R,r,eps0=8.854187817e-12):
    if r>R:
        V=A_VOut(rho,n,R,r,eps0)
    else:
        V=A_VIn(rho,n,R,r,eps0)
    return V
# Electric field out
def A_EOut(rho,n,R,r,eps0):
    E=rho*R*R*n/2./(n+2.)/eps0/r
    return E
# Electric field inside
def A_EIn(rho,n,R,r,eps0):
    E=rho*r/eps0*(.5-math.pow(r,n)/(n+2)/math.pow(R,n))
    return E
# Electric field everywhere
def A_E(rho,n,R,r,eps0=8.854187817e-12):
    if r>R:
        E=A_EOut(rho,n,R,r,eps0)
    else:
        E=A_EIn(rho,n,R,r,eps0)
    return E


# Case B
# Long non-conductor cylinder with charge density
# \rho(r)=\rho_0\left(\frac{r}{R}\right)^n
# Potential out
def B_VOut(rho,n,R,r,eps0):
    V=-rho*R*R/eps0/(n+2.)*math.log(r/R)
    return V
# Potential inside
def B_VIn(rho,n,R,r,eps0):
    V=rho*(math.pow(R,2.)-math.pow(r,n+2.)/math.pow(R,n))/eps0/math.pow(n+2,2.)
    return V
# Potential everywhere
def B_V(rho,n,R,r,eps0=8.854187817e-12):
    if r>R:
        V=B_VOut(rho,n,R,r,eps0)
    else:
        V=B_VIn(rho,n,R,r,eps0)
    return V
# Electric field out
def B_EOut(rho,n,R,r,eps0):
    E=rho*R*R/eps0/(n+2.)/r
    return E
# Electric field inside
def B_EIn(rho,n,R,r,eps0):
    E=rho*math.pow(r,n+1.)/eps0/math.pow(R,n)/(n+2)
    return E
# Electric field everywhere
def B_E(rho,n,R,r,eps0=8.854187817e-12):
    if r>R:
        return B_EOut(rho,n,R,r,eps0)
    else:
        return B_EIn(rho,n,R,r,eps0)

# Usually, you call this 
# random.seed()
# But we need a fixed seed for repeatibility (regression tests)
random.seed(123897977)

# Input parameters to formatted string: sgnstr,rho,n,R,r
AtextV="A long non-conducting cylinder is charged with a non-uniform charge density given by $$\\rho(r)=\\rho_0\\left [1-\\left( \{r \\over R\}  \\right )^n\\right ]$$ with <em>&rho;<sub>0</sub>=</em>%s%.4g C/m<sup>3</sup>, <em>n</em>=%d and <em>R</em>=%.4g m. Which is the electric potential at a distance <em>r</em>=%.4g m from the axis if the potential is taken as zero at the surface of the cylinder?"
AtextE="A long non-conducting cylinder is charged with a non-uniform charge density given by $$\\rho(r)=\\rho_0\\left [1-\\left( \{r \\over R\}  \\right )^n\\right ]$$ with <em>&rho;<sub>0</sub>=</em>%s%.4g C/m<sup>3</sup>, <em>n</em>=%d and <em>R</em>=%.4g m. Which is the magnitude of the electric field at a distance <em>r</em>=%.4g m from the axis?"
BtextV="A long non-conducting cylinder is charged with a non-uniform charge density given by $$\\rho(r)=\\rho_0\\left [1-\\left( \{r \\over R\}  \\right )^n\\right ]$$ with <em>&rho;<sub>0</sub>=</em>%s%.4g C/m<sup>3</sup>, <em>n</em>=%d and <em>R</em>=%.4g m. Which is the electric potential at a distance <em>r</em>=%.4g m from the axis if the potential is taken as zero at the surface of the cylinder?"
BtextE="A long non-conducting cylinder is charged with a non-uniform charge density given by $$\\rho(r)=\\rho_0\\left [1-\\left( \{r \\over R\}  \\right )^n\\right ]$$ with <em>&rho;<sub>0</sub>=</em>%s%.4g C/m<sup>3</sup>, <em>n</em>=%d and <em>R</em>=%.4g m. Which is the magnitude of the electric field at a distance <em>r</em>=%.4g from the axis?"

rhos=[10,0.5,.1,.36,5.]
ns=[3.,4.,5.,6.,7.]
Rs=[0.01,0.05,0.33,0.5,0.75,1.55,2.,4.,6.]
Ss=[-1,1]

# For each radius nQuestions questions
nQuestions=2
# Full ordinal of question
iQ=0
for R in Rs:
    rs=[]
    rs.append(R/5.)
    rs.append(R/2.)
    rs.append(R*1.25)
    rs.append(R*3.)
    rs.append(R*5.)
    rpars=RandomInputParameters()
    rpars.AppendParameters(rhos)
    rpars.AppendParameters(ns)
    rpars.AppendParameters([R])
    rpars.AppendParameters(rs)
    rpars.AppendParameters(Ss)
    tol=0.075
    for iq in range(nQuestions):
        plist=rpars.GetParameterList()
        if len(plist)==0:
            sys.stderr.write("// No questions left after %d\n"%(iq,))
            break
        rho=plist[0]*plist[4]
        rho=plist[0]
        n=plist[1]
        R=plist[2]
        r=plist[3]
        sgn=plist[4]
        category="pyGIFT/ElectroMagnetism/Electrostatics/GaussianSurfaces/NQ"
        retval=A_V(rho,n,R,r)
        if sgn>0 :
           sgnstr="&plus;"
        if sgn<0 :
           sgnstr="&minus;"
        qtext=AtextV%(sgnstr,rho,n,R,r)
        qname="Gauss_Atype_Vpotential_%2.2d"%(iQ,)
        BuildNumQuestion(category,qname,qtext,"V",retval)
        retval=abs(A_E(rho,n,R,r))
        qtext=AtextE%(sgnstr,rho,n,R,r)
        qname="Gauss_Atype_Efield_%2.2d"%(iQ,)
        BuildNumQuestion(category,qname,qtext,"V/m",retval)
        retval=B_V(rho,n,R,r)
        qtext=BtextV%(sgnstr,rho,n,R,r)
        qname="Gauss_Btype_Vpotential_%2.2d"%(iQ,)
        BuildNumQuestion(category,qname,qtext,"V",retval)
        retval=abs(B_E(rho,n,R,r))
        qtext=BtextE%(sgnstr,rho,n,R,r)
        qname="Gauss_Btype_Efield_%2.2d"%(iq,)
        BuildNumQuestion(category,qname,qtext,"V/m",retval)
        iQ+=1

