# EM_Magnetic_Field_MC.py
# 
# A generator of multiple choice questions about Magnetic Field
# computed by using Ampere's law by means of pygiftgenerator.py
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# The copyright of this software corresponds to its authors, 
# Jon Saenz, 
# Idoia G. Gurtubay, 
# Zunbeltz Izaola, 
# employees at the University of the Basque Country, 
# Universidad del Pais Vasco/Euskal Herriko Unibertsitatea (UPV/EHU).
#
# Code released in 2020 under GPL 3.
# For details, see https://www.gnu.org/licenses/gpl.html
# and https://www.gnu.org/licenses/gpl.txt
# 

import math,sys,random

# Import the constructor of questions
from pygiftgenerator import BuildMulChoiceQuestion

# And the randomizers for parameter inputs
from pygiftgenerator import RandomInputParameters


# Usually, you call this 
# random.seed()
# But we need a fixed seed for repeatibility (regression tests)
random.seed(123897977)

# Parameters are built from these lists
Is=[0.5,.75,.9,1,1.2,1.5,1.7,2.5,3.,3.3,4.4,5,5.6,7.]
Ss=[-1,1]
As=[0.01,0.05,0.1,0.25,0.5,0.7,0.8,1,1.2,1.3,1.4]
Bs=[2.3,2.5,2.7,2.8,2.9,3,3.5,4.,4.6,7.8,9.]

# Random parameters 
randIntParams=RandomInputParameters()
randIntParams.AppendParameters(Is)
randIntParams.AppendParameters(Is)
randIntParams.AppendParameters(Ss)
randIntParams.AppendParameters(Ss)
randIntParams.AppendParameters(As)
randIntParams.AppendParameters(Bs)
####

nQuestions=10
category="pyGIFT/ElectroMagnetism/MagneticField/MC"
PI=math.acos(-1.)
mu0=4*PI*1.0e-7
for iq in range(nQuestions):
    plist=randIntParams.GetParameterList()
    if len(plist)==0:
        sys.stderr.write("// No questions left after %d\n"%(iq,))
        break
    I1=plist[0]
    I2=plist[1]
    S1=plist[2]
    if S1>0:
        s1str="upward"
    else:
        s1str="downward"
    S2=plist[3]
    if S2>0:
        s2str="upward"
    else:
        s2str="downward"
    a=plist[4]
    b=plist[5]
    question="[html]Intensity I<sub>1</sub>=%.3g A flows %s while current I<sub>2</sub>=%.3g A flows %s through parallel long wires, as shown in figure. The distance from every wire to the center of coordinates is given by a\=%.3g m. Calculate the total magnetic field <b>B</b> at point P, placed at a distance b\=%.3g m measured from the center of coordinates.<p><img src\\=\"http://www.ehu.eus/eolo/pyGIFT/Bfields.png\" height=\"180\" width=\"186\" ></p>"%(I1,s1str,I2,s2str,a,b)
    qName="Bfields_%3.3d_MC"%(iq,)
    weights=[100,-5,-5,-5,-5]
    # This calculates the right solution as a number
    correct=mu0/2/PI*((I1*S1)/(b+a)+(I2*S2)/(b-a))
    # These are the wrong ones
    w1=mu0/2/PI*((I1*S1)/(b-a)+(I2*S2)/(b+a))
    w2=mu0/2/PI*((I1*S1)/(b+a)-(I2*S2)/(b-a))
    w3=mu0/2/PI*((I1*S1)/(b-a)-(I2*S2)/(b+a))
    w4=mu0/2/PI*((I2*S1)/(b-a)+(I1*S2)/(b+a))
    # Prepare nice looking string answers with units
    ansFmt="%.3g  <b>j</b> T"
    stranswers=[ansFmt%(correct,),
              ansFmt%(w1,),
              ansFmt%(w2,),
              ansFmt%(w3,),
              ansFmt%(w4,) ]
    # Just in case, check whether there are repeated answers and do not
    # let them go into the GIFT file, as valid answers
    sRep=True
    BuildMulChoiceQuestion(category,qName,question,stranswers,weights,sRep)

