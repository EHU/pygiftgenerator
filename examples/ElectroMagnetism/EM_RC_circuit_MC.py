#EM_RC_circuit_MC.py
#
# pygiftgenerator.py
# 
# A generator of numerical and multiple choice Moodle-based questions
# with the aim of covering large areas of physics teaching
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# The copyright of this software corresponds to its authors, 
# Jon Saenz, 
# Idoia G. Gurtubay, 
# Zunbeltz Izaola, 
# employees at the University of the Basque Country, 
# Universidad del Pais Vasco/Euskal Herriko Unibertsitatea (UPV/EHU).
#
# Code released in 2020 under GPL 3.
# For details, see https://www.gnu.org/licenses/gpl.html
# and https://www.gnu.org/licenses/gpl.txt
# 

import math
import pygiftgenerator as pyg

# Find the potential in the capacitor
question_Vc= "<p>The battery in the circuit (potential-difference V=%d V) is charging (switch S connected to point a) a capacitor C=%.2e F through resistor R<sub>1</sub>=%.1e &Omega;. A second resistor R<sub>2</sub>=%.1e &Omega; can be connected for discharge as shown in the figure (switch at b). Find the potential difference in the capacitor, V<sub>C</sub> at time t=%.2f s if the capacitor was initially discharged.</p> <p><img src\\=\"http://www.ehu.eus/eolo/pyGIFT/CircuitRC.png\" height=\"195\" width=\"449\" ></p>"

# Find the energy dissipated in R1
question_E1= "<p>The battery in the circuit (potential-difference V=%d V) is charging (switch S connected to point a) a capacitor C=%.2e F through resistor R<sub>1</sub>=%.1e &Omega;. A second resistor R<sub>2</sub>=%.1e &Omega; can be connected for discharge as shown in the figure (switch at b). Find the energy dissipated in the resistor R<sub>1</sub> at time t=%.2f s if the capacitor was initially discharged.</p> <p><img src\\=\"http://www.ehu.eus/eolo/pyGIFT/CircuitRC.png\" height=\"195\" width=\"449\" ></p>"

# Find the  intensity through I2 during discharge
question_I2= "<p>The battery in the circuit (potential-difference V=%d V) has fully charged (switch  connected to  a) a capacitor C=%.2e F through resistor R<sub>1</sub>=%.1e &Omega;. A second resistor R<sub>2</sub>=%.1e &Omega; can be connected (switch at b) for discharge as shown in the figure. Find the intensity through the resistor R<sub>2</sub> %.2f s after changing the switch to position b.</p> <p><img src\\=\"http://www.ehu.eus/eolo/pyGIFT/CircuitRC.png\" height=\"195\" width=\"449\" ></p>"

# Parameters are built from this list. If we keep the for loops
# these parameter combination yields 288 questions
#V0s=[6,8,11]
#R1s=[1.5e5, 3.3e05]
#R2s=[2.2e5,4.7e05]
#Cs=[ 22.0e-6,47.0e-6 ]

# Make the parameters shorter for the example
V0s=[6]
R1s=[1.5e5, 3.3e05]
R2s=[2.2e5]
Cs=[ 22.0e-6, 47.0e-6 ]



iQ=1
weights=[100,-5,-5,-5,-5]
category="pyGIFT/ElectroMagnetism/Circuits/RC/MC"

for V0 in V0s:
    for C in Cs:
        for R1 in R1s:
            tau1=R1*C
            for R2 in R2s:
                tau2=R2*C
                #for t in [.35*tau1,0.6*tau1,1.2*tau1,1.6*tau1]:
                for t in [.35*tau1,0.6*tau1]:
                    # Potential in the capacitor (Vc)
                    q=question_Vc%(V0,C,R1,R2,t)
                    correct=V0*(1.0-math.exp(-t/tau1))
                    wrong1= V0*(1.0-math.exp(-t/tau2))
                    wrong2= V0*(1.0-math.exp(-t/(tau1+tau2)))
                    wrong3= V0*(math.exp(-t/tau1))
                    wrong4= V0*(math.exp(-t/(tau1+tau2)))
                    answers=[correct,wrong1,wrong2,wrong3,wrong4]
                    weights=[100,-5,-5,-5,-5]
                    qname="RC_GetVC_ChargeMC%4.4d"%(iQ,)
                    sanswers=[]
                    for theAns in answers:
                          sanswers.append("%9.3g V"%(theAns,))
                    QQ=pyg.GIFTMulChoiceQ(category,qname,q,sanswers,weights)
                    QQ.DumpGift()
                    
                    # Energy lost in R1
                    q=question_E1%(V0,C,R1,R2,t)
                    correct=V0*V0*C/2.*(1.0-math.exp(-2*t/tau1))
                    wrong1= V0*C/2.*(1.0-math.exp(-2*t/tau1))
                    wrong2= V0*V0*C/2.*(1.0-math.exp(-2*t/(tau1+tau2)))
                    wrong3= V0*V0/R1*(math.exp(-2*t/tau1))
                    wrong4= V0*V0/(R1+R2)*(math.exp(-2*t/(tau1+tau2)))
                    answers=[correct,wrong1,wrong2,wrong3,wrong4]
                    weights=[100,-5,-5,-5,-5]
                    qname="RC_GetE1_ChargeMC%4.4d"%(iQ,)
                    sanswers=[]
                    for theAns in answers:
                          sanswers.append("%8.2e J"%(theAns,))
                    QQ=pyg.GIFTMulChoiceQ(category,qname,q,sanswers,weights)
                    QQ.DumpGift()

                    #Intensity flowing through R2 at discharge
                    q=question_I2%(V0,C,R1,R2,t)
                    correct=V0/R2*(math.exp(-t/tau2))
                    wrong1= V0/R1*(math.exp(-t/tau1))
                    wrong2= V0/(R1+R2)*(math.exp(-t/(tau1+tau2)))
                    wrong3= V0/R2*(1-math.exp(-t/tau2))
                    wrong4= V0/(R1+R2)*(1-math.exp(-t/(tau1+tau2)))
                    answers=[correct,wrong1,wrong2,wrong3,wrong4]
                    weights=[100,-5,-5,-5,-5]
                    qname="RC_GetI2_dischargeMC%4.4d"%(iQ,)
                    sanswers=[]
                    for theAns in answers:
                          #sanswers.append("%9.2g A"%(theAns,))
                          sanswers.append("%8.2e A"%(theAns,))
                    QQ=pyg.GIFTMulChoiceQ(category,qname,q,sanswers,weights)
                    QQ.DumpGift()

                    iQ+=1

