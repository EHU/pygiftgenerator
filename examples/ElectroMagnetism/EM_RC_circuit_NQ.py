# EM_RC_circuit_NQ.py
# 
# Application of pyGIFTGeneration to some questions related to
# Resistor-Capacitor circuits producing NQ (Numerical Questions)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# The copyright of this software corresponds to its authors, 
# Jon Saenz, 
# Idoia G. Gurtubay, 
# Zunbeltz Izaola, 
# employees at the University of the Basque Country, 
# Universidad del Pais Vasco/Euskal Herriko Unibertsitatea (UPV/EHU).
#
# Code released in 2020 under GPL 3.
# For details, see https://www.gnu.org/licenses/gpl.html
# and https://www.gnu.org/licenses/gpl.txt
# 
import math
import random

# Import the constructor of numerical questions
from pygiftgenerator import GIFTNumQ
# And the randomizers for parameter inputs
from pygiftgenerator import RandomInputParameters


# Calculate the potential in the capacitor
class GET_VC_Charge(GIFTNumQ):
    # Provide a constructor with the needed data (the ones that are passed
    # to the default constructor and teh new ones needed in this case
    def __init__(self,rootCat,qName,question,units,tol,V,R1,C,t):
        GIFTNumQ.__init__(self,rootCat,qName,question,units,tol)
        self.V=V
        self.R1=R1
        self.C=C
        self.tau1=R1*C
        self.t=t
    # This method calculates the solution and is the expected one
    # that needs being provided by this intance so that the
    # inherited class works
    def Correct(self):
        return self.V*(1.-math.exp(-self.t/self.tau1))

# Calculate the energy provided by the battery from t=0 to t
class GET_Energy_Charge(GIFTNumQ):
    # Provide a constructor with the needed data (the ones that are passed
    # to the default constructor and teh new ones needed in this case
    def __init__(self,rootCat,qName,question,units,tol,V,R1,C,t):
        GIFTNumQ.__init__(self,rootCat,qName,question,units,tol)
        self.V=V
        self.R1=R1
        self.C=C
        self.tau1=R1*C
        self.t=t
    # This method calculates the solution and is the expected one
    # that needs being provided by this intance so that the
    # inherited class works
    def Correct(self):
        return 0.5*self.V*self.V*self.C*(1.-math.exp(-2*self.t/self.tau1))
    
# Calculate the intensity flowing through R2 during discharge at time t
class GET_IR2_Discharge(GIFTNumQ):
    # Provide a constructor with the needed data (the ones that are passed
    # to the default constructor and teh new ones needed in this case
    def __init__(self,rootCat,qName,question,units,tol,V,R2,C,t):
        GIFTNumQ.__init__(self,rootCat,qName,question,units,tol)
        self.V=V
        self.R2=R2
        self.C=C
        self.tau2=R2*C
        self.t=t
    # This method calculates the solution and is the expected one
    # that needs being provided by this intance so that the
    # inherited class works
    def Correct(self):
        return self.V/self.R2*math.exp(-self.t/self.tau2)


# Usually, you call this 
# random.seed()
# But we need a fixed seed for repeatibility (regression tests)
random.seed(123897977)

# Parameter list
Vs=[6,8,11]
R1s=[1.5e5, 3.3e05]
R2s=[2.2e5,4.7e05]
Cs=[ 22.0e-6,33.0e-06,47.0e-6 ]


# Random parameters 
randIntParams=RandomInputParameters()
randIntParams.AppendParameters(Vs)
randIntParams.AppendParameters(R1s)
randIntParams.AppendParameters(R2s)
randIntParams.AppendParameters(Cs)
####

category="pyGIFT/ElectroMagnetism/Circuits/RC/NQ"
nQuestions= 5
tol=0.075
for iq in range(nQuestions):
    plist=randIntParams.GetParameterList()
    if len(plist)==0:
        print("// No questions left after ",iq)
        break
    V=plist[0]
    R1=plist[1]
    R2=plist[2]
    C=plist[3]
    tau1=R1*C
    count=1
    #for tVal in [.35*tau1,0.6*tau1,1.2*tau1,1.6*tau1]:
    for tVal in [.35*tau1,0.6*tau1]:
        # V in capacitor
        question= "<p>The battery in the circuit (potential-difference V=%d V) is charging (switch S connected to point a) a capacitor C=%.2e F through resistor R<sub>1</sub>=%.1e &Omega;. A second resistor R<sub>2</sub>=%.1e &Omega; can be connected for discharge as shown in the figure (switch at b). Find the potential difference in the capacitor, V<sub>C</sub> at time t=%.2f s if the capacitor was initially discharged.</p> <p><img src\\=\"http://www.ehu.eus/eolo/pyGIFT/CircuitRC.png\" height=\"195\" width=\"449\" ></p>"%(V,C,R1,R2,tVal)
        qName="RC_GetVC_Charge_%3.3d_%d"%(iq,count)
        q=GET_VC_Charge(category,qName,question,"V",tol,V,R1,C,tVal)
        q.DumpGift()
        # Energy provided by battery during charge
        question= "<p>The battery in the circuit (potential-difference V=%d V) is charging (switch S connected to point a) a capacitor C=%.2e F through resistor R<sub>1</sub>=%.1e &Omega;. A second resistor R<sub>2</sub>=%.1e &Omega; can be connected for discharge as shown in the figure (switch at b). Find the total energy provided by the battery during the charging process from time 0 s to time t=%.2f s if the capacitor was initially discharged.</p> <p><img src\\=\"http://www.ehu.eus/eolo/pyGIFT/CircuitRC.png\" height=\"195\" width=\"449\" ></p>"%(V,C,R1,R2,tVal)
        qName="RC_GetEnergy_Charge_%3.3d_%d"%(iq,count)
        q=GET_Energy_Charge(category,qName,question,"J",tol,V,R1,C,tVal)
        q.DumpGift()
        # Same values for discharge
        # Intensity flowing through R2 during discharge
        question= "<p>The battery in the circuit (potential-difference V=%d V) has fully charged (switch  connected to  a) a capacitor C=%.2e F through resistor R<sub>1</sub>=%.1e &Omega;. A second resistor R<sub>2</sub>=%.1e &Omega; can be connected (switch at b) for discharge as shown in the figure. Find the intensity through the resistor R<sub>2</sub> %.2f s after changing the switch to position b.</p> <p><img src\\=\"http://www.ehu.eus/eolo/pyGIFT/CircuitRC.png\" height=\"195\" width=\"449\" ></p>"%(V,C,R1,R2,tVal)
        qName="RC_GetIR2_Discharge_%3.3d_%d"%(iq,count)
        q=GET_IR2_Discharge(category,qName,question,"A",tol,V,R2,C,tVal)
        q.DumpGift()
        count+=1

