
# Gravitational interaction between two stars with masses of the same
# order of magnitude, so that students have to
# use reduced mass, effective potential energy and so on.
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# The copyright of this software corresponds to its authors, 
# Jon Saenz, 
# Idoia G. Gurtubay, 
# Zunbeltz Izaola, 
# employees at the University of the Basque Country, 
# Universidad del Pais Vasco/Euskal Herriko Unibertsitatea (UPV/EHU).
#
# Code released in 2020 under GPL 3.
# For details, see https://www.gnu.org/licenses/gpl.html
# and https://www.gnu.org/licenses/gpl.txt
# 
import math
import random
import numpy as np
import pygiftgenerator as pyg

# Usually, you call this 
# random.seed()
# But we need a fixed seed for repeatibility (regression tests)
random.seed(123897977)

# Constants that we need
G=6.67408e-11
Msun=1.989e30
M=Msun
AU=149597870700.0 #m = 1 a.u.

# All questions will be output into this file
ofile=open("ME_binary_star_MC.gift","w")

# Question Type 3. Given $n$, $m$ and $r_0$ calculate the
# semi-major axis of the orbit.
# 
# The instructor can calculate the energy of the orbit and it can
# be shown that the orbit will be closed if and only if
# $n>\frac{m\left(m+2\right)}{2}$. Keeping this condition when
# the parameters for the questions are selected:
# 
# + Calculate the energy of the orbit
# $E=-\frac{n\left(1+2n-m\left(m+2\right)\right)}{2\left(n+1\right)} \frac{GM^2}{r_0}$.
# + Equate the energy of the orbit to: $E=-\frac{GnM^2}{2a}$
# 
# $$
# a=\frac{\left(n+1\right) r_0}{\left(1+2n-m\left(m+2\right)\right)}
# $$

# This time, build again the questions using functions for a
# Multiple-Choice kind of question but using functions
# This builds a list of answers [correct, wrong, wrong, wrong, wrong]
# r0 is input as AU, so, outputs are expressed in
# astronomical units in all cases
def BuildAnswers(r0,n,m):
    # Numbers needed to build the list of answers
    # Right answer
    a=(n+1.)*r0/(1.+2*n-m*(m+2.))
    # First wrong: student answers c
    eccent=math.sqrt(1+(math.pow(m+1.,2.)*(m*(m+2.)-2.*n-1.))/math.pow(n+1.,2.))
    c=eccent*a
    # Second wrong: Student answers b
    b=math.sqrt(a*a-c*c)
    # Third and fourth errors. Students mix the definitions of and and c
    # with rp/ra
    ra=(a+c)/2.
    rp=(a-c)/2.
    # List of answers (same order weights)
    answers=[ "%.2f AU"%(a,),
             "%.2f AU"%(c,),
             "%.2f AU"%(b,),
             "%.2f AU"%(ra,),
             "%.2f AU"%(rp,)
            ]
    return answers


ms=np.arange(1.,10.,.5)
nds=np.arange(5.,20,1.)
r0s=np.arange(5,30,2.5)
pars=pyg.RandomInputParameters()
pars.AppendParameters(ms)
pars.AppendParameters(nds)
pars.AppendParameters(r0s)

# Correct 100%, wrong aswers always get -5% of the grade
weights=[100,-5,-5,-5,-5]

# This defines the format to be used for the conversion parameters
# -> problem text
questionFmt="[html]The figure shows the initial conditions for a "+\
    "system of two Sun-like stars (masses \(m_1=nM\) and \(m_2=M\))."+\
    " \(M=1.989\\times 10^\{30\}\) kg is the mass of the Sun and "+\
    "\(n\)=%d. They are orbiting at a distance \(r_0\)=%5.1f AU "+\
    "apart with  velocities, as detected from a distant laboratory "+\
    "(at rest), \( v_1=v_0=\\sqrt\{\\frac\{GM\}\{r_0\}\}\) approaching "+\
    "the laboratory, and \(v_2=mv_0\) receding, with \(m=%.1f\). Which "+\
    "is the orbit's semi-major axis? "+\
    "<p><img src\\=\"http://www.ehu.eus/eolo/pyGIFT/masak-pygift.png\" height=\"197\" width=\"595\" ></p>"
# Category where this question is to be nested
rootCategory="pyGIFT/Mechanics/CentralForces/MC"
# Number of questions to generate
nQuestions=20
# Counter for questions
iq=0
# Iterate to generate the questions
while iq<nQuestions:
    # Randomly extract parameters from the parameter list given
    parset=pars.GetParameterList()
    if parset is None:
        # The elements in the input list have already finished
        # Too many extractions from a small input dataset
        # Not able to extract as many questions
        print("Can not extract more different parameter set from input lists when iq =",iq)
        break
    m,n,r0=parset
    nmin=(m*(m+2.)-1.)/2.
    if n<=nmin:
        # This case can not be solved, open orbit, skip it
        print("Skipping case n/m ",n,m,"because orbit is open")
        continue
    else:
        print("Accepted case n/m:",n,m)
    qName="mechanics-Central-Forces-QT3-%3.3d"%(iq,)
    question=questionFmt%(int(n),r0,m)
    answers=BuildAnswers(r0,int(n),m)
    # Create a Multiple-choice question
    pyg.BuildMulChoiceQuestion(rootCategory,qName,question,answers,weights,
                           True,ofile)
    iq=iq+1
# Close output file
ofile.close()




