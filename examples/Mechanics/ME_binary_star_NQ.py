# ME_binary_star_NQ.py
#
# Gravitational interaction between two stars in a binary system
# Masses are similar, students must use concepts such as reduced mass,
# effective potential and so on
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# The copyright of this software corresponds to its authors, 
# Jon Saenz, 
# Idoia G. Gurtubay, 
# Zunbeltz Izaola, 
# employees at the University of the Basque Country, 
# Universidad del Pais Vasco/Euskal Herriko Unibertsitatea (UPV/EHU).
#
# Code released in 2020 under GPL 3.
# For details, see https://www.gnu.org/licenses/gpl.html
# and https://www.gnu.org/licenses/gpl.txt
# 

import math
import random
import numpy as np
import pygiftgenerator as pyg

# Usually, you call this 
# random.seed()
# But we need a fixed seed for repeatibility (regression tests)
random.seed(123897977)

# Constants commonly used
G=6.67408e-11
Msun=1.989e30
M=Msun
AU=149597870700.0 #m = 1 a.u


# All questions will be output into this file
ofile=open("ME_binary_star_NQ.gift","w")


# Type 1. Given $m$, get $n$ so that the orbit is circular.
# 
# Procedure:
# 
# + Calculate the reduced mass $\mu$ and the angular momentum
# $L$ of the reduced mass using the relative position and velocity.
#
# + Calculate the derivative with $r$ of the effective potential energy
# $V_e=-\frac{GnM^2}{r}+\frac{L^2}{2\mu r^2}$ $V_e'$, and solve
# for $\left . V_e'\right|_{r_0}=0$
# 
# Solution: $n=m(m+2)$
def Q1Answer(m):
    return m*(m+2.)


# Set of input parameters to be used for Type I questions
ms=np.arange(1.,10.,.5)
nds=np.arange(5.,20,1.)
r0s=np.arange(5,30,1.5)
pars=pyg.RandomInputParameters()
pars.AppendParameters(ms)
pars.AppendParameters(nds)
pars.AppendParameters(r0s)

# This defines the format to be used for the conversion parameters -> problem text
questionFmt="[html]The figure shows the initial conditions for a system of "+\
    "two Sun-like stars (masses \(m_1=nM\) and \(m_2=M\)). "+\
    "\(M=1.989\\times 10^\{30\}\) kg is the mass of the Sun and \(n\) "+\
    "a positive constant. They are orbiting at a distance "+\
    "\(r_0\)=%5.1f AU apart with  velocities, as detected from a "+\
    "distant laboratory (at rest), "+\
    "\( v_1=v_0=\\sqrt\{\\frac\{GM\}\{r_0\}\}\) approaching the "+\
    "laboratory, and \(v_2=mv_0\) receding, with \(m=%.1f\). "+\
    "Which is the value of \(n\) if the orbit is circular? "+\
    "<p><img src\\=\"http://www.ehu.eus/eolo/pyGIFT/masak-pygift.png\" height=\"197\" width=\"595\" ></p>"
# Units of the answer (non-dimensional in this case)
units=""
# Category where this question is to be nested
rootCategory="pyGIFT/Mechanics/CentralForces/NQ"
# Number of questions to generate
nQuestions=20
# Counter for questions
iq=0
# Iterate to generate the questions
while iq<nQuestions:
    # Randomly extract parameters from the parameter list given
    parset=pars.GetParameterList()
    if len(parset)==0:
        # The elements in the input list have already finished
        # Too many extractions from a small input dataset
        # Not able to extract as many questions, empty list is returned
        print("Can not extract more different parameter set from input lists when iq =",iq)
        break
    m,nd,r0=parset
    # This is the answer to question Q1
    n=Q1Answer(m)
    nmin=(m*(m+2.)-1.)/2.
    if n<=nmin:
        # This case can not be solved, open orbit, skip it
        print("Skipping case n/m ",n,m,"because orbit is open")
        continue
    qName="mechanics-Central-Forces-QT1-%3.3d"%(iq,)
    pyg.BuildNumQuestion(rootCategory,qName,questionFmt%(r0,m),"",n,ofileObject=ofile)
    iq=iq+1
      
        


# Type 2. Given $n$ and $m$, calculate the eccentricity of the orbit.
# 
# The instructor can calculate the energy of the orbit and it can be
# shown that the orbit will be closed if and only if
# $n>\frac{m\left(m+2\right)}{2}$. Keeping this condition
# when the parameters for the questions are selected:
# 
# 
# + Apply the expression $\varepsilon=\sqrt{1+\frac{2L^2E}{\mu G^2M^2n^2}}$
# + Substitute values for $E$ and $L$ to yield 
# $$
# \varepsilon=\sqrt{\frac{\left(n+1\right)^2+\left(m+1\right)^2\left(m\left(m+2\right)-2n-1\right)}{\left(n+1\right)^2}}
# $$
# This time, build the questions using inheritance of base question objects
# to overload dumpGift, pass only the parameters actually needed (m,n)
# for computations
class QType2(pyg.GIFTNumQ):
    def __init__(self,rootCat,qName,question,units,tol,ofile,m,n):
        pyg.GIFTNumQ.__init__(self,rootCat,qName,question,units,tol,ofileObject=ofile)
        self.m=m
        self.n=n
    # This is called due to inheritance
    def Correct(self):
        eccent=math.sqrt(1.+(math.pow((self.m+1.),2.)*(self.m*(self.m+2.)-2.*self.n-1.))/math.pow((self.n+1.),2.))
        return eccent

# Input parameters for Type II questions (recycle the ones above
# to reset random extraction from the full list)
ms=np.arange(1.,10.,.5)
nds=np.arange(5.,20,1.)
r0s=np.arange(5,30,2.5)
pars=pyg.RandomInputParameters()
pars.AppendParameters(ms)
pars.AppendParameters(nds)
pars.AppendParameters(r0s)

# This defines the format to be used for the conversion parameters ->
# problem text
questionFmt="[html]The figure shows the initial conditions for a system "+\
    "of two Sun-like stars (masses \(m_1=nM\) and \(m_2=M\)). "+\
    "\(M=1.989\\times 10^\{30\}\) kg is the mass of the Sun and \(n\)=%d. "+\
    "They are orbiting at a distance \(r_0\)=%5.1f AU apart with  "+\
    "velocities, as detected from a distant laboratory (at rest), "+\
    "\( v_1=v_0=\\sqrt\{\\frac\{GM\}\{r_0\}\}\) approaching the "+\
    "laboratory, and \(v_2=mv_0\) receding, with \(m=%.1f\). "+\
    "Which is the eccentricity of the orbit? "+\
    "<p><img src\\=\"http://www.ehu.eus/eolo/pyGIFT/masak-pygift.png\" height=\"197\" width=\"595\" ></p>"
# Units of the answer adimensional for epsilon
units=""
# Category where this question is to be nested
rootCategory="pyGIFT/Mechanics/CentralForces/NQ"
# Number of questions to generate
nQuestions=20
# Counter for questions
iq=0
# Tolerance accepted in the response
tol=0.05
# Iterate to generate the questions
while iq<nQuestions:
    # Randomly extract parameters from the parameter list given
    parset=pars.GetParameterList()
    if parset is None:
        # The elements in the input list have already finished
        # Too many extractions from a small input dataset
        # Not able to extract as many questions
        print("Can not extract more different parameter set from input lists when iq =",iq)
        break
    m,n,r0=parset
    nmin=(m*(m+2.)-1.)/2.
    if n<=nmin:
        # This case can not be solved, open orbit, skip it
        print("Skipping case n/m ",n,m,"because orbit is open")
        continue
    else:
        print("Accepted case n/m:",n,m)
    qName="mechanics-Central-Forces-QT2-%3.3d"%(iq,)
    question=questionFmt%(int(n),r0,m)
    # Build an instance of this class
    q=QType2(rootCategory,qName,question,units,tol,ofile,m,n)
    q.DumpGift()
    iq=iq+1

# Type 4. Given $𝑛$, $𝑚$, $r_0$ and the mass of the star $M$,
# calculate the period of the orbit.
# 
# The instructor can calculate the energy of the orbit and it can be shown
# that the orbit will be closed if and only if $𝑛>𝑚\left(𝑚+2\right)2$.
# Keeping this condition when the parameters for the questions are selected:
# 
# + Calculate the semi-major axis of the orbit (see Type 3 above).
# + Apply the generalized expression for the Third Kepler's law to
# the reduced mass in this problem:
# $\frac{T^2}{a^3}=\frac{4\pi^2}{G M \left(n+1\right)}$
# 
# $$
# T=\sqrt{
# \frac{4\pi^2}{GM}
# \frac{\left(n+1\right)^2r_0^3}{\left(1+2n-m\left(m+2\right) \right)^3}
# }
# $$

# This time, build the questions using inheritance of base question objects
# to overload DumpGift, pass only the parameters actually needed (m,n)
# for computations
class QType4(pyg.GIFTNumQ):
    def __init__(self,rootCat,qName,question,units,tol,ofile,m,n,r0,M,G):
        pyg.GIFTNumQ.__init__(self,rootCat,qName,question,units,tol,ofileObject=ofile)
        self.m=m
        self.n=n
        self.r0=r0
        self.M=M
        self.G=G
    # This is called due to inheritance
    def Correct(self):
        # Reduced mass
        mu=float(self.n)/(self.n+1.)*self.M
        # The constant for the potential energy as in JMA's V=-k/r
        k=self.G*self.M*self.M*self.n
        # Angular momentum
        L=(self.m+1.)*self.n/(self.n+1.)*math.sqrt(self.G*self.M*self.M*self.M*self.r0)
        # Energy for this orbit
        E=L*L/2/mu/self.r0/self.r0-self.G*self.M*self.M*self.n/self.r0
        a=-self.G*self.M*self.M*self.n/2./E
        # Period
        T=math.sqrt(4*math.pow(math.acos(-1.),2)*math.pow(a,3.)/self.G/(self.M+self.n*self.M))
        return T/86400./365.25

# Input parameters
ms=np.arange(1.,10.,.5)
nds=np.arange(5.,20,1.)
r0s=np.arange(5,30,2.5)
Ms=np.arange(0.5,10,.5)*Msun
pars=pyg.RandomInputParameters()
pars.AppendParameters(ms)
pars.AppendParameters(nds)
pars.AppendParameters(r0s)
pars.AppendParameters(Ms)

# This defines the format to be used for the conversion parameters ->
# problem text
questionFmt="[html]The figure shows the initial conditions for a system "+\
    "of two Sun-like stars (masses <em>m<sub>1</sub>=nM</em> and "+\
    "<em>m<sub>2</sub>=M</em> with <em>M</em>=%.2e kg and </em>n</em>=%d). "+\
    "They are orbiting at a distance </em>r<sub>0</sub></em>=%5.1f AU "+\
    "apart with  velocities, as detected from a distant laboratory (at "+\
    "rest), \( v_1=v_0=\\sqrt\{\\frac\{GM\}\{r_0\}\}\) approaching the "+\
    "laboratory, and <em>v<sub>2</sub>=mv<sub>0</sub></em> receding, "+\
    "with <em>m</em>=%.1f. Which is the orbital period? Note: "+\
    "Take the terrestrial year as 365.25 days. "+\
    "<p><img src\\=\"http://www.ehu.eus/eolo/pyGIFT/masak-pygift.png\" height=\"197\" width=\"595\" ></p>"
# Units of the answer adimensional for epsilon
units="terrestrial years"
# Category where this question is to be nested
rootCategory="pyGIFT/Mechanics/CentralForces/NQ"
# Number of questions to generate
nQuestions=20
# Counter for questions
iq=0
# Tolerance accepted in the response
tol=0.05
# Iterate to generate the questions
while iq<nQuestions:
    # Randomly extract parameters from the parameter list given
    parset=pars.GetParameterList()
    if parset is None:
        # The elements in the input list have already finished
        # Too many extractions from a small input dataset
        # Not able to extract as many questions
        print("Can not extract more different parameter set from input lists when iq =",iq)
        break
    m,n,r0,Mstar=parset
    nmin=(m*(m+2.)-1.)/2.
    if n<=nmin:
        # This case can not be solved, open orbit, skip it
        print("Skipping case n/m ",n,m,"because orbit is open")
        continue
    else:
        print("Accepted case n/m:",n,m)
    qName="mechanics-Central-Forces-QT4-%3.3d"%(iq,)
    question=questionFmt%(Mstar,int(n),r0,m)
    # Build an instance of this class
    q=QType4(rootCategory,qName,question,units,tol,ofile,m,n,r0*AU,Mstar,G)
    q.DumpGift()
    iq=iq+1

# Close output file
ofile.close()
