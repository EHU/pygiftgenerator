# ME_coupled_pulleys_MC.py
#
# pygiftgenerator.py
# 
# A generator of numerical and multiple choice Moodle-based questions
# with the aim of covering large areas of physics teaching
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# The copyright of this software corresponds to its authors, 
# Jon Saenz, 
# Idoia G. Gurtubay, 
# Zunbeltz Izaola, 
# employees at the University of the Basque Country, 
# Universidad del Pais Vasco/Euskal Herriko Unibertsitatea (UPV/EHU).
#
# Code released in 2020 under GPL 3.
# For details, see https://www.gnu.org/licenses/gpl.html
# and https://www.gnu.org/licenses/gpl.txt
# 

import math
import random
import pygiftgenerator as pyg

# Once the system is solved for alpha, the other magnitudes
# are computed as a function of alpha
def getT1(alpha,m1,r1,g):
    return m1*(g+alpha*r1)
def getT2(alpha,m2,r2,g):
    return m2*(g-alpha*r2)
def geta(alpha,r):
    return alpha*r

# Usually, you call this 
# random.seed()
# But we need a fixed seed for repeatibility (regression tests)
random.seed(123897977)


# Find the angular acceleration
questionFmt_alpha="<p>We form a block by coupling two pulleys that spin around a common axis.  The radii of the pulleys are r<sub>1</sub>=%.1f m and r<sub>2</sub>=%.1f m. We hang a %.1f-kg mass (m<sub>1</sub>) from the biggest pulley, while a second %.1f-kg mass m<sub>2</sub> hangs from the smallest pulley in the opposite side. This way, pulleys tend to rotate in opposite directions. The moment of inertia of the set of pulleys is %.1f kg m<sup>2</sup>. The system is released from rest. The arrow in the figure denotes the positive direction of rotation. Find the angular acceleration of the system.</p><br><p><img src\\=\"http://www.ehu.eus/eolo/pyGIFT/fig_coupled_pulleys.png\" height=\"185\" width=\"112\" ></p>"

# Find T1
questionFmt_T1="<p>We form a block by coupling two pulleys that spin around a common axis.  The radii of the pulleys are r<sub>1</sub>=%.1f m and r<sub>2</sub>=%.1f m. We hang a %.1f-kg mass (m<sub>1</sub>) from the biggest pulley, while a second %.1f-kg mass m<sub>2</sub> hangs from the smallest pulley in the opposite side. This way, pulleys tend to rotate in opposite directions. The moment of inertia of the set of pulleys is %.1f kg m<sup>2</sup>. The system is released from rest. The arrow in the figure denotes the positive direction of rotation. Find the tension on the string attached to mass m<sub>1</sub>.</p><br><p><img src\\=\"http://www.ehu.eus/eolo/pyGIFT/fig_coupled_pulleys.png\" height=\"185\" width=\"112\" ></p>"

# Find T2
questionFmt_T2="<p>We form a block by coupling two pulleys that spin around a common axis.  The radii of the pulleys are r<sub>1</sub>=%.1f m and r<sub>2</sub>=%.1f m. We hang a %.1f-kg mass (m<sub>1</sub>) from the biggest pulley, while a second %.1f-kg mass m<sub>2</sub> hangs from the smallest pulley in the opposite side. This way, pulleys tend to rotate in opposite directions. The moment of inertia of the set of pulleys is %.1f kg m<sup>2</sup>. The system is released from rest. The arrow in the figure denotes the positive direction of rotation. Find the tension on the string attached to mass m<sub>2</sub>.</p><br><p><img src\\=\"http://www.ehu.eus/eolo/pyGIFT/fig_coupled_pulleys.png\" height=\"185\" width=\"112\" ></p>"


# Find a1
questionFmt_a1="<p>We form a block by coupling two pulleys that spin around a common axis.  The radii of the pulleys are r<sub>1</sub>=%.1f m and r<sub>2</sub>=%.1f m. We hang a %.1f-kg mass (m<sub>1</sub>) from the biggest pulley, while a second %.1f-kg mass m<sub>2</sub> hangs from the smallest pulley in the opposite side. This way, pulleys tend to rotate in opposite directions. The moment of inertia of the set of pulleys is %.1f kg m<sup>2</sup>. The system is released from rest. The arrow in the figure denotes the positive direction of rotation. Find the acceleration of mass m<sub>1</sub>.</p><br><p><img src\\=\"http://www.ehu.eus/eolo/pyGIFT/fig_coupled_pulleys.png\" height=\"185\" width=\"112\" ></p>"


# Find a2
questionFmt_a2="<p>We form a block by coupling two pulleys that spin around a common axis.  The radii of the pulleys are r<sub>1</sub>=%.1f m and r<sub>2</sub>=%.1f m. We hang a %.1f-kg mass (m<sub>1</sub>) from the biggest pulley, while a second %.1f-kg mass m<sub>2</sub> hangs from the smallest pulley in the opposite side. This way, pulleys tend to rotate in opposite directions. The moment of inertia of the set of pulleys is %.1f kg m<sup>2</sup>. The system is released from rest. The arrow in the figure denotes the positive direction of rotation. Find the acceleration of mass m<sub>2</sub>.</p><br><p><img src\\=\"http://www.ehu.eus/eolo/pyGIFT/fig_coupled_pulleys.png\" height=\"185\" width=\"112\" ></p>"


# Parameters are built from these lists
# These are the original parameters in the NQ but they give 1280 combinations
# at the end of the for loops (for each question)
#m1_list=[20, 18, 16, 14]
#m2_list=[100, 110, 120, 130]
#r1_list=[1, 1.2, 1.4, 1.6]
#r2_list=[0.3, 0.4, 0.5, 0.6]
#I_list=[10, 12, 14, 8, 6]

# Let's make them shorter
m1_list=[15.,18.,20.]
m2_list=[100., 110.,120.]
r1_list=[1.2, 1.4 , 1.6 ]
r2_list=[0.3, 0.4,0.5,0.6 ]
I_list=[10., 12., 14. ,16.]

# Randomly extract parameters
rps=pyg.RandomInputParameters()
rps.AppendParameters(m1_list)
rps.AppendParameters(m2_list)
rps.AppendParameters(r1_list)
rps.AppendParameters(r2_list)
rps.AppendParameters(I_list)

                              
g=9.81
# For every type
nQuestions=6 
# weights assigned to the answers
weights=[100,-5,-5,-5,-5]
# Category
category="pyGIFT/Mechanics/Dynamics/Systems/RotationTranslation/MC"

# Instead of randomly picking parameters,
# they can be all of them extracted from lists, too,
# but the number of generated questions usually tends to be very
# large
# for m1 in m1_list:
#    for m2 in m2_list:
#        for r1 in r1_list:
#            for r2 in r2_list:
#                for I in I_list:
for iQ in range(nQuestions):
    m1,m2,r1,r2,I=rps.GetParameterList()
    # Make sure we are not dividing by zero
    if (m1*r1*r1+m2*r2*r2-I) !=0  :
        # ALPHAs
        q=questionFmt_alpha%(r1,r2,m1,m2,I)
        alpha_c=g*(m2*r2-m1*r1)/(m1*r1*r1+m2*r2*r2+I)
        alpha_w1=g*(m2*r2-m1*r1)/(m1*r1*r1+m2*r2*r2-I)
        alpha_w2= g*(m2-m1)/(m1+m2)/r1
        alpha_w3=g*(m2*r2+m1*r1)/(m1*r1*r1+m2*r2*r2-I)
        alpha_w4=g*(-m2*r2+m1*r1)/(m1*r1*r1+m2*r2*r2-I)
        answers=["%5.1f rad s<sup>-2</sup>"%(alpha_c,),
                 "%5.1f rad s<sup>-2</sup>"%(alpha_w1,),
                 "%5.1f rad s<sup>-2</sup>"%(alpha_w2,),
                 "%5.1f rad s<sup>-2</sup>"%(alpha_w3,),
                 "%5.1f rad s<sup>-2</sup>"%(alpha_w4,)
        ]
        Q=pyg.GIFTMulChoiceQ(category,"get_alpha_MC%4.4d"%(iQ,),q,
                             answers,weights)
        Q.DumpGift()

        # T1
        q=questionFmt_T1%(r1,r2,m1,m2,I)
        t1_c= getT1(alpha_c  ,m1,r1,g)
        t1_w1=getT1(alpha_w1,m1,r1,g)
        t1_w2=getT1(alpha_w2,m1,r1,g)
        t1_w3=getT1(alpha_w3,m1,r1,g)
        t1_w4=getT1(alpha_w4,m1,r1,g)
        # Don't use those giving negative tensions
        if t1_c>0 and t1_w1>0 and t1_w2>0 and t1_w3>0 and t1_w4>0 :
            answers=["%7.1f N"%(t1_c,),
                     "%7.1f N"%(t1_w1,),
                     "%7.1f N"%(t1_w2,),
                     "%7.1f N"%(t1_w3,),
                     "%7.1f N"%(t1_w4,)
            ] 
            Q=pyg.GIFTMulChoiceQ(category,"get_T1_MC%4.4d"%(iQ,),q,
                                 answers,weights)
            Q.DumpGift()

        # T2
        q=questionFmt_T2%(r1,r2,m1,m2,I)
        t2_c = getT2(alpha_c  ,m2,r2,g)
        t2_w1= getT2(alpha_w1,m2,r2,g)
        t2_w2= getT2(alpha_w2,m2,r2,g)
        t2_w3= getT2(alpha_w3,m2,r2,g)
        t2_w4= getT2(alpha_w4,m2,r2,g)
        # Don't use those giving negative tensions
        if t2_c>0 and t2_w1>0 and t2_w2>0 and t2_w3>0 and t2_w4>0 :
            answers=["%7.1f N"%(t2_c,),
                     "%7.1f N"%(t2_w1,),
                     "%7.1f N"%(t2_w2,),
                     "%7.1f N"%(t2_w3,),
                     "%7.1f N"%(t2_w4,)
            ] 
            Q=pyg.GIFTMulChoiceQ(category,"get_T2_MC%4.4d"%(iQ,),q,
                                 answers,weights)
            Q.DumpGift()

        # a1
        q=questionFmt_a1%(r1,r2,m1,m2,I)
        a1_c =geta(alpha_c, r1)
        a1_w1=geta(alpha_w1,r1)
        a1_w2=geta(alpha_w2,r1)
        a1_w3=geta(alpha_w3,r1)
        a1_w4=geta(alpha_w4,r1)
        answers=["%7.1f m s<sup>-2</sup>"%(a1_c,),
                 "%7.1f m s<sup>-2</sup>"%(a1_w1,),
                 "%7.1f m s<sup>-2</sup>"%(a1_w2,),
                 "%7.1f m s<sup>-2</sup>"%(a1_w3,),
                 "%7.1f m s<sup>-2</sup>"%(a1_w4,)
        ] 
        Q=pyg.GIFTMulChoiceQ(category,"get_a1_MC%4.4d"%(iQ,),q,
                             answers,weights)
        Q.DumpGift()
                       
        # a2
        q=questionFmt_a2%(r1,r2,m1,m2,I)
        a2_c = geta(alpha_c, r2)
        a2_w1= geta(alpha_w1,r2)
        a2_w2= geta(alpha_w2,r2)
        a2_w3= geta(alpha_w3,r2)
        a2_w4= geta(alpha_w4,r2)
        answers=["%7.1f m s<sup>-2</sup>"%(a2_c,),
                 "%7.1f m s<sup>-2</sup>"%(a2_w1,),
                 "%7.1f m s<sup>-2</sup>"%(a2_w2,),
                 "%7.1f m s<sup>-2</sup>"%(a2_w3,),
                 "%7.1f m s<sup>-2</sup>"%(a2_w4,)
        ] 
        Q=pyg.GIFTMulChoiceQ(category,"get_a2_MC%4.4d"%(iQ,),q,
                             answers,weights)
        Q.DumpGift()

