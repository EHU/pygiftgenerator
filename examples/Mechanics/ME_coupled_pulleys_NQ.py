# ME_coupled_pulleys_NQ.py
#
# pygiftgenerator.py
# 
# A generator of numerical and multiple choice Moodle-based questions
# with the aim of covering large areas of physics teaching
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# The copyright of this software corresponds to its authors, 
# Jon Saenz, 
# Idoia G. Gurtubay, 
# Zunbeltz Izaola, 
# employees at the University of the Basque Country, 
# Universidad del Pais Vasco/Euskal Herriko Unibertsitatea (UPV/EHU).
#
# Code released in 2020 under GPL 3.
# For details, see https://www.gnu.org/licenses/gpl.html
# and https://www.gnu.org/licenses/gpl.txt
# 


import math
import random

# Import the constructor of questions
from pygiftgenerator import GIFTNumQ
# And the randomizers for parameter inputs
from pygiftgenerator import RandomInputParameters

# Find the angular acceleration
class GET_ALPHA(GIFTNumQ):
    # Provide a constructor with the needed data (the ones that are passed
    # to the default constructor and teh new ones needed in this case
    def __init__(self,rootCat,qName,question,units,tol,m1,m2,r1,r2,I):
        GIFTNumQ.__init__(self,rootCat,qName,question,units,tol)
        self.m1=m1
        self.m2=m2
        self.r1=r1
        self.r2=r2
        self.I=I
        self.g=9.81
        self.alpha=self.g*(self.m2*self.r2-self.m1*self.r1)/(self.m1*self.r1*self.r1+self.m2*self.r2*self.r2+self.I)
    # This method calculates the solution and is the expected one
    # that needs being provided by this intance so that the
    # inherited class works
    def Correct(self):
        return self.alpha


# Find T1
class GET_TENSION_1(GIFTNumQ):
    # Provide a constructor with the needed data (the ones that are passed
    # to the default constructor and teh new ones needed in this case
    def __init__(self,rootCat,qName,question,units,tol,m1,m2,r1,r2,I):
        GIFTNumQ.__init__(self,rootCat,qName,question,units,tol)
        self.m1=m1
        self.m2=m2
        self.r1=r1
        self.r2=r2
        self.I=I
        self.g=9.81
        self.alpha=self.g*(self.m2*self.r2-self.m1*self.r1)/(self.m1*self.r1*self.r1+self.m2*self.r2*self.r2+self.I)
        self.T1=m1*(self.g+self.alpha*self.r1)
    # This method calculates the solution and is the expected one
    # that needs being provided by this intance so that the
    # inherited class works
    def Correct(self):
        return self.T1

# Find T2
class GET_TENSION_2(GIFTNumQ):
    # Provide a constructor with the needed data (the ones that are passed
    # to the default constructor and teh new ones needed in this case
    def __init__(self,rootCat,qName,question,units,tol,m1,m2,r1,r2,I):
        GIFTNumQ.__init__(self,rootCat,qName,question,units,tol)
        self.m1=m1
        self.m2=m2
        self.r1=r1
        self.r2=r2
        self.I=I
        self.g=9.81
        self.alpha=self.g*(self.m2*self.r2-self.m1*self.r1)/(self.m1*self.r1*self.r1+self.m2*self.r2*self.r2+self.I)
        self.T2=m2*(self.g-self.alpha*self.r2)
    # This method calculates the solution and is the expected one
    # that needs being provided by this intance so that the
    # inherited class works
    def Correct(self):
        return self.T2


# Find a1
class GET_ACCEL_1(GIFTNumQ):
    # Provide a constructor with the needed data (the ones that are passed
    # to the default constructor and teh new ones needed in this case
    def __init__(self,rootCat,qName,question,units,tol,m1,m2,r1,r2,I):
        GIFTNumQ.__init__(self,rootCat,qName,question,units,tol)
        self.m1=m1
        self.m2=m2
        self.r1=r1
        self.r2=r2
        self.I=I
        self.g=9.81
        self.alpha=self.g*(self.m2*self.r2-self.m1*self.r1)/(self.m1*self.r1*self.r1+self.m2*self.r2*self.r2+self.I)
        self.a1=self.alpha*self.r1
    # This method calculates the solution and is the expected one
    # that needs being provided by this intance so that the
    # inherited class works
    def Correct(self):
        return self.a1

# Find a2
class GET_ACCEL_2(GIFTNumQ):
    # Provide a constructor with the needed data (the ones that are passed
    # to the default constructor and teh new ones needed in this case
    def __init__(self,rootCat,qName,question,units,tol,m1,m2,r1,r2,I):
        GIFTNumQ.__init__(self,rootCat,qName,question,units,tol)
        self.m1=m1
        self.m2=m2
        self.r1=r1
        self.r2=r2
        self.I=I
        self.g=9.81
        self.alpha=self.g*(self.m2*self.r2-self.m1*self.r1)/(self.m1*self.r1*self.r1+self.m2*self.r2*self.r2+self.I)
        self.a2=self.alpha*self.r2
    # This method calculates the solution and is the expected one
    # that needs being provided by this intance so that the
    # inherited class works
    def Correct(self):
        return self.a2

# Usually, you call this 
# random.seed()
# But we need a fixed seed for repeatibility (regression tests)
random.seed(123897977)

# Parameters are built from these lists
m1_list=[20, 18, 16, 14]
m2_list=[100, 110, 120, 130]
r1_list=[1, 1.2, 1.4, 1.6]
r2_list=[0.3, 0.4, 0.5, 0.6]
I_list=[10, 12, 14, 8, 6]

# Random parameters 
randIntParams=RandomInputParameters()
randIntParams.AppendParameters(m1_list)
randIntParams.AppendParameters(m2_list)
randIntParams.AppendParameters(r1_list)
randIntParams.AppendParameters(r2_list)
randIntParams.AppendParameters(I_list)
####

nQuestions=25
# Tolerance is set slightly larger than the difference between using g=9.81 or g=10
# Same tolerance for all questions
category="pyGIFT/Mechanics/Dynamics/Systems/RotationTranslation/NQ"
tol=0.025
for iq in range(nQuestions):
    plist=randIntParams.GetParameterList()
    if len(plist)==0:
        print("// No questions left after ",iq)
        break
    m1=plist[0]
    m2=plist[1]
    r1=plist[2]
    r2=plist[3]
    I=plist[4]
    # Do not build different questions with the same parameters
    if 0<=iq<5:
       question="We form a block by coupling two pulleys that spin around a common axis. The radii of the pulleys are r<sub>1</sub>=%.1f m and r<sub>2</sub>=%.1f m.  We hang a %.1f-kg mass (m<sub>1</sub>) from the biggest pulley, while a second %.1f-kg mass m<sub>2</sub> hangs from the smallest pulley in the opposite side. This way, pulleys tend to rotate in opposite directions.  The moment of inertia of the set of pulleys is %.1f kg m<sup>2</sup>. The system is released from rest. The arrow in the figure denotes the positive direction of rotation. Find the angular acceleration of the system. <p><img src\\=\"http://www.ehu.eus/eolo/pyGIFT/fig_coupled_pulleys.png\" height=\"185\" width=\"112\" ></p>"%(r1,r2,m1,m2,I)
       qName="pulleys_Get_alpha_NQ%3.3d"%(iq,)
       q=GET_ALPHA(category,qName,question,"rad/s<sup>2</sup>",tol,m1,m2,r1,r2,I)
    
    elif 5<=iq<10:
       question="We form a block by coupling two pulleys that spin around a common axis. The radii of the pulleys are r<sub>1</sub>=%.1f m and r<sub>2</sub>=%.1f m.  We hang a %.1f-kg mass (m<sub>1</sub>) from the biggest pulley, while a second %.1f-kg mass m<sub>2</sub> hangs from the smallest pulley in the opposite side. This way, pulleys tend to rotate in opposite directions. The moment of inertia of the set of pulleys is %.1f kg m<sup>2</sup>. The system is released from rest. The arrow in the figure denotes the positive direction of rotation. Find the tension on mass m<sub>1</sub>. <p><img src\\=\"http://www.ehu.eus/eolo/pyGIFT/fig_coupled_pulleys.png\" height=\"185\" width=\"112\" ></p>"%(r1,r2,m1,m2,I)
       qName="pulleys_Get_T1_NQ%3.3d"%(iq,)
       q=GET_TENSION_1(category,qName,question,"N",tol,m1,m2,r1,r2,I)

    elif 10<=iq<15:
       question="We form a block by coupling two pulleys that spin around a common axis. The radii of the pulleys are r<sub>1</sub>=%.1f m and r<sub>2</sub>=%.1f m.  We hang a %.1f-kg mass (m<sub>1</sub>) from the biggest pulley, while a second %.1f-kg mass m<sub>2</sub> hangs from the smallest pulley in the opposite side. This way, pulleys tend to rotate in opposite directions. The moment of inertia of the set of pulleys is %.1f kg m<sup>2</sup>. The system is released from rest. The arrow in the figure denotes the positive direction of rotation. Find the tension on mass m<sub>2</sub>. <p><img src\\=\"http://www.ehu.eus/eolo/pyGIFT/fig_coupled_pulleys.png\" height=\"185\" width=\"112\" ></p>"%(r1,r2,m1,m2,I)
       qName="pulleys_Get_T2_NQ%3.3d"%(iq,)
       q=GET_TENSION_2(category,qName,question,"N",tol,m1,m2,r1,r2,I)

    elif 15<=iq<20:
       question="We form a block by coupling two pulleys that spin around a common axis. The radii of the pulleys are r<sub>1</sub>=%.1f m and r<sub>2</sub>=%.1f m.  We hang a %.1f-kg mass (m<sub>1</sub>) from the biggest pulley, while a second %.1f-kg mass m<sub>2</sub> hangs from the smallest pulley in the opposite side. This way, pulleys tend to rotate in opposite directions. The moment of inertia of the set of pulleys is %.1f kg m<sup>2</sup>. The system is released from rest. The arrow in the figure denotes the positive direction of rotation. Find the acceleration of mass m<sub>1</sub>. <p><img src\\=\"http://www.ehu.eus/eolo/pyGIFT/fig_coupled_pulleys.png\" height=\"185\" width=\"112\" ></p>"%(r1,r2,m1,m2,I)
       qName="pulleys_Get_a1_NQ%3.3d"%(iq,)
       q=GET_ACCEL_1(category,qName,question,"m/s<sup>2</sup>",tol,m1,m2,r1,r2,I)

    else:
       question="We form a block by coupling two pulleys that spin around a common axis. The radii of the pulleys are r<sub>1</sub>=%.1f m and r<sub>2</sub>=%.1f m.  We hang a %.1f-kg mass (m<sub>1</sub>) from the biggest pulley, while a second %.1f-kg mass m<sub>2</sub> hangs from the smallest pulley in the opposite side. This way, pulleys tend to rotate in opposite directions. The moment of inertia of the set of pulleys is %.1f kg m<sup>2</sup>. The system is released from rest. The arrow in the figure denotes the positive direction of rotation. Find the acceleration of mass m<sub>2</sub>. <p><img src\\=\"http://www.ehu.eus/eolo/pyGIFT/fig_coupled_pulleys.png\" height=\"185\" width=\"112\" ></p>"%(r1,r2,m1,m2,I)
       qName="pulleys_Get_a2_NQ%3.3d"%(iq,)
       q=GET_ACCEL_2(category,qName,question,"m/s<sup>2</sup>",tol,m1,m2,r1,r2,I)

    q.DumpGift()


    
