# ME_kinematics_MC.py
#
# pygiftgenerator.py
# 
# A generator of numerical and multiple choice Moodle-based questions
# with the aim of covering large areas of physics teaching
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# The copyright of this software corresponds to its authors, 
# Jon Saenz, 
# Idoia G. Gurtubay, 
# Zunbeltz Izaola, 
# employees at the University of the Basque Country, 
# Universidad del Pais Vasco/Euskal Herriko Unibertsitatea (UPV/EHU).
#
# Code released in 2020 under GPL 3.
# For details, see https://www.gnu.org/licenses/gpl.html
# and https://www.gnu.org/licenses/gpl.txt
# 

# Import the constructor of questions
import pygiftgenerator as pyg
# We need the mathematical functions to calculate
import math

# These will be the input parameters for all the cases
# Using for loops with these parameters produce 102 questions
#alphas=[2.,3.,4.,5.]
#betas=[3,4,5]
#v0s=[20.,25.,30.,40.]
#ts=[1,1.5,2.]

# Shorter input parameters
alphas=[2.,3.]
betas=[3,4,5]
v0s=[20.,25.]
ts=[1,1.5,2.]



# This category
category="pyGIFT/Mechanics/Kinematics/PointMass/MC"
iQ=1

for a in alphas:
    for b in betas:
        for v0 in v0s:
            for t in ts:
                question="[html]An astronaut arrives to a recently-found planet where the gravitational acceleration is given by a\=%4.1f t<sup>%d</sup> m/s<sup>2</sup>. She throws an object upwards with vertical speed v<sub>0</sub>\=%5.1f m/s. Find its vertical position after %3.1f s."%(a,b,v0,t)
                correct=v0*t-float(a)/float((b+2)*(b+1.))*math.pow(t,b+2.)
                wrong1=v0*t-.5*(a*math.pow(t,b))*math.pow(t,2.)
                wrong2=v0*t
                wrong3=v0*t+float(a)/float((b+2)*(b+1.))*math.pow(t,b+2.)
                wrong4=float(a)/float((b+2)*(b+1.))*math.pow(t,b+2.)
                # Dealing with distances. Select questions with positive answers  
                if correct >0  and wrong1>0 and wrong2>0 and wrong3 >0 and wrong4>0 :
                   answers=[correct,wrong1,wrong2,wrong3,wrong4]
                   sanswers=[]
                   weights=[100,-5,-5,-5,-5]
                   for theAns in answers:
                       sanswers.append("%9.2g m"%(theAns,))
                   QQ=pyg.GIFTMulChoiceQ(category,"kinematics%4.4d"%(iQ,),question,
                                         sanswers,weights)
                   QQ.DumpGift()
                   iQ+=1
