# ME_plane_motor_NQ.py
# 
# pygiftgenerator.py
# 
# A generator of numerical and multiple choice Moodle-based questions
# with the aim of covering large areas of physics teaching
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# The copyright of this software corresponds to its authors, 
# Jon Saenz, 
# Idoia G. Gurtubay, 
# Zunbeltz Izaola, 
# employees at the University of the Basque Country, 
# Universidad del Pais Vasco/Euskal Herriko Unibertsitatea (UPV/EHU).
#
# Code released in 2020 under GPL 3.
# For details, see https://www.gnu.org/licenses/gpl.html
# and https://www.gnu.org/licenses/gpl.txt
# 
#

import math
import random 

# Import the constructor of questions
from pygiftgenerator import GIFTNumQ
# And the randomizers for parameter inputs
from pygiftgenerator import RandomInputParameters

# Find the power developed by the motor
class GET_POWER(GIFTNumQ):
    # Provide a constructor with the needed data (the ones that are passed
    # to the default constructor and teh new ones needed in this case
    def __init__(self,rootCat,qName,question,units,tol,vB,m,d,hB):
        GIFTNumQ.__init__(self,rootCat,qName,question,units,tol)
        self.vB=vB
        self.m=m
        self.d=d
        self.hB=hB
        self.g=9.81
        self.vA=3.5
        self.emA=0.5*self.m*self.vA*self.vA
        self.emB=0.5*self.m*self.vB*self.vB+self.m*self.g*self.hB
        self.time=2.0*self.d/(self.vA+self.vB)  
        self.power=(self.emB-self.emA)/self.time
    # This method calculates the solution and is the expected one
    # that needs being provided by this intance so that the
    # inherited class works
    def Correct(self):
        return self.power

# Find the work done by the motor
class GET_WORK(GIFTNumQ):
    # Provide a constructor with the needed data (the ones that are passed
    # to the default constructor and teh new ones needed in this case
    def __init__(self,rootCat,qName,question,units,tol,vB,m,d,hB):
        GIFTNumQ.__init__(self,rootCat,qName,question,units,tol)
        self.vB=vB
        self.m=m
        self.d=d
        self.hB=hB
        self.g=9.81
        self.vA=3.5
        self.emA=0.5*self.m*self.vA*self.vA
        self.emB=0.5*self.m*self.vB*self.vB+self.m*self.g*self.hB
        self.work=(self.emB-self.emA)
    # This method calculates the solution and is the expected one
    # that needs being provided by this intance so that the
    # inherited class works
    def Correct(self):
        return self.work

# Gvien the power find the distance up the incline (d)
class GET_DISTANCE(GIFTNumQ):
    # Provide a constructor with the needed data (the ones that are passed
    # to the default constructor and teh new ones needed in this case
    def __init__(self,rootCat,qName,question,units,tol,vB,m,hB,p):
        GIFTNumQ.__init__(self,rootCat,qName,question,units,tol)
        self.vB=vB
        self.m=m
        self.p=p
        self.hB=hB
        self.g=9.81
        self.vA=3.5
        self.emA=0.5*self.m*self.vA*self.vA
        self.emB=0.5*self.m*self.vB*self.vB+self.m*self.g*self.hB
        self.work=(self.emB-self.emA)
        self.den=2*self.p/(self.vA+self.vB)
        self.distance=self.work/self.den
    # This method calculates the solution and is the expected one
    # that needs being provided by this intance so that the
    # inherited class works
    def Correct(self):
        return self.distance

class GET_HEIGHT(GIFTNumQ):
    # Provide a constructor with the needed data (the ones that are passed
    # to the default constructor and teh new ones needed in this case
    def __init__(self,rootCat,qName,question,units,tol,vB,m,d,p):
        GIFTNumQ.__init__(self,rootCat,qName,question,units,tol)
        self.vB=vB
        self.m=m
        self.p=p
        self.d=d
        self.g=9.81
        self.vA=3.5
        self.ekA=0.5*self.m*self.vA*self.vA
        self.ekB=0.5*self.m*self.vB*self.vB
        self.height=(self.p*2.0*self.d/(self.vA+self.vB)-(self.ekB-self.ekA))/self.m/self.g
    # This method calculates the solution and is the expected one
    # that needs being provided by this intance so that the
    # inherited class works
    def Correct(self):
        return self.height


    
# Usually, you call this 
# random.seed()
# But we need a fixed seed for repeatibility (regression tests)
random.seed(123897977)

# Parameters are built from these lists
vB_list=[4,4.5,5.5,6,6.5,7]
m_list=[2,2.5,3,3.5,4,4.5,5]
d_list=[30,32,34,36,38,40]
hB_list=[2.2, 2.6, 3, 3.4, 3.6]
power_list=[ 15, 16, 17, 18  ]

# Random parameters 
randIntParams=RandomInputParameters()
randIntParams.AppendParameters(vB_list)
randIntParams.AppendParameters(m_list)
randIntParams.AppendParameters(d_list)
randIntParams.AppendParameters(hB_list)
randIntParams.AppendParameters(power_list)
####

nQuestions=28
category="pyGIFT/Mechanics/Dynamics/PointMass/NQ"
# Change the default tolerance
tol=0.075
for iq in range(nQuestions):
    plist=randIntParams.GetParameterList()
    if len(plist)==0:
        print("// No questions left after ",iq)
        break
    vB=plist[0]
    m=plist[1]
    d=plist[2]
    hB=plist[3]
    p=plist[4]
    # Do not build the different questions time with the same parameters
    if 0<=iq<7:
        question="The object in the figure has a mass of m=%.1f kg and is pulled up a slope AB, which is d=%.1f m long; the height BC is h<sub>B</sub>=%.1f m.  There is no friction and the acceleration is constant.  The speed v<sub>A</sub> at A is 3.5 m/s whereas the speed at B is v<sub>B</sub>=%.1f m/s.  Find the average power developed by the motor pulling the object. <p><img src\\=\"http://www.ehu.eus/eolo/pyGIFT/fig_inclined_plane.png\" height=\"126\" width=\"486\" ></p>"%(m,d,hB,vB)
        qName="motor_GetPower_NQ%3.3d"%(iq,)
        q=GET_POWER(category,qName,question,"Watt",tol,vB,m,d,hB)
    elif 7<=iq<14:
        question="The object in the figure has a mass of m=%.1f kg and is pulled up a slope AB, which is d=%.1f m long; the height BC is h<sub>B</sub>=%.1f m.  There is no friction and the acceleration is constant.  The speed v<sub>A</sub> at A is 3.5 m/s whereas the speed at B is v<sub>B</sub>=%.1f m/s.  Find the work done by the motor pulling the object. <p><img src\\=\"http://www.ehu.eus/eolo/pyGIFT/fig_inclined_plane.png\" height=\"126\" width=\"486\" ></p>"%(m,d,hB,vB)
        qName="motor_GetWork_NQ%3.3d"%(iq,)
        q=GET_WORK(category,qName,question,"J",tol,vB,m,d,hB)
    elif 14<=iq<21:
        question="The object in the figure has a mass of m=%.1f kg and is pulled up a slope AB, by a motor of power P=%.1f Watt; the height BC is h<sub>B</sub>=%.1f m.  There is no friction and the acceleration is constant.  The speed v<sub>A</sub> at A is 3.5 m/s whereas the speed at B is v<sub>B</sub>=%.1f m/s.  Find the distance AB. <p><img src\\=\"http://www.ehu.eus/eolo/pyGIFT/fig_inclined_plane.png\" height=\"126\" width=\"486\" ></p>"%(m,p,hB,vB)
        qName="motor_GetDistance_NQ%3.3d"%(iq,)
        q=GET_DISTANCE(category,qName,question,"m",tol,vB,m,hB,p)
    else:
        question="The object in the figure has a mass of m=%.1f kg and is pulled up a slope AB, which is d=%.1f m by a motor of power P=%.1f Watt;  There is no friction and the acceleration is constant.  The speed v<sub>A</sub> at A is 3.5 m/s whereas the speed at B is v<sub>B</sub>=%.1f m/s.  Find the height BC of the incline. <p><img src\\=\"http://www.ehu.eus/eolo/pyGIFT/fig_inclined_plane.png\" height=\"126\" width=\"486\" ></p>"%(m,d,p,vB)
        qName="motor_GetHeight_NQ%3.3d"%(iq,)
        q=GET_HEIGHT(category,qName,question,"m",tol,vB,m,d,p)
    q.DumpGift()


    
