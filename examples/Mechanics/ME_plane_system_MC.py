# ME_plane_system_MC.py
#
# pygiftgenerator.py
# 
# A generator of numerical and multiple choice Moodle-based questions
# with the aim of covering large areas of physics teaching
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# The copyright of this software corresponds to its authors, 
# Jon Saenz, 
# Idoia G. Gurtubay, 
# Zunbeltz Izaola, 
# employees at the University of the Basque Country, 
# Universidad del Pais Vasco/Euskal Herriko Unibertsitatea (UPV/EHU).
#
# Code released in 2020 under GPL 3.
# For details, see https://www.gnu.org/licenses/gpl.html
# and https://www.gnu.org/licenses/gpl.txt
# 


import math
import random
import pygiftgenerator as pyg

def deg2rad(d):
    return d/180.*math.acos(-1.)


# Usually, you call this 
# random.seed()
# But we need a fixed seed for repeatibility (regression tests)
random.seed(123897977)

questionFmt="The enclosed figure shows a mass m<sub>1</sub>\=%5.1f kg on top of an inclined plane with angle &alpha;\=%3.1f&deg; and friction coefficient &mu;\=%4.2f. It is tied through a massless string to a mass m<sub>2</sub>\=%5.1f kg. The string passes through a pulley of mass m\=%5.1f kg and radius R\=%3.1f m. Which is the acceleration of the system?<br><p><img src\\=\"http://www.ehu.eus/eolo/pyGIFT/masak-plano-txirrika.png\" height=\"145\" width=\"196\" ></p>"

# This combination of parameter lists always make m1 go up the inclined plane. 
# In case you change the parameters, the if condition below makes sure
# that only questions making  m1 go up the inclined plane are considered.
# You can increase the number of elements in the list
# and the number of questions increases accordingly
# Thus, for demonstration purposes, we set the lists to two or one
# elements, but there is no restriction on that lengths
m1s=[1.,1.5,2.,2.5]
m2s=[4.0,4.2,4.5,5.0,5.5,6.0]
mus=[0.15,0.2] 
ms=[0.7,0.9]
Rs=[0.1,0.15,0.2]
alphas=[20.,30.,45.,60.]
# You could also use a RandonInputSelector object, but then,
# the condition for masses producing acceleration to the right must
# be enforced for the solution to be valid

# Random selector of parameters
rps=pyg.RandomInputParameters()
rps.AppendParameters(m1s)
rps.AppendParameters(m2s)
rps.AppendParameters(mus)
rps.AppendParameters(ms)
rps.AppendParameters(Rs)
rps.AppendParameters(alphas)
#######################################

                     
g=9.81
# Extract nQuestion items
nQuestion=20
category="pyGIFT/Mechanics/Dynamics/Systems/RotationTranslation/MC"
weights=[100,-5,-5,-5,-5]
# Instead of iterating over all the elements
# which is also possible, but it produces a too high number
# of questions
# for m1 in m1s:
#    for m2 in m2s:
#        for mu in mus:
#            for m in ms:
#                for R in Rs:
#                    for alpha in alphas:
for iQ in range(nQuestion):
    m1,m2,mu,m,R,alpha=rps.GetParameterList()
    ralpha=deg2rad(alpha)
    q=questionFmt%(m1,alpha,mu,m2,m,R)
    # Make sure (m2-m1*(math.sin(ralpha)+mu*math.cos(ralpha)))>0
    # so that m1 goes up the incline
    if (m2-m1*(math.sin(ralpha)+mu*math.cos(ralpha)))>0 :
        correct=2*g*(m2-m1*(math.sin(ralpha)+mu*math.cos(ralpha)))/(2*(m1+m2)+m)
        wrong1=2*(m2+m1*(math.sin(ralpha)+mu*math.cos(ralpha)))*g/(2*(m1+m2)+m)
        wrong2=2*(m2-m1*(math.sin(ralpha)+mu*math.cos(ralpha)))*g/(2*(m1+m2))
        wrong3=9.81
        wrong4=2*(m2-m1*(math.sin(ralpha)-mu*math.cos(ralpha)))/(2*(m1+m2)+m)
        answers=["%5.1f m s<sup>-2</sup>"%(correct,),
                 "%5.1f m s<sup>-2</sup>"%(wrong1,),
                 "%5.1f m s<sup>-2</sup>"%(wrong2,),
                 "%5.1f m s<sup>-2</sup>"%(wrong3,),
                 "%5.1f m s<sup>-2</sup>"%(wrong4,)
        ]
        Q=pyg.GIFTMulChoiceQ(category,"planesystem_get_a%3.3d"%(iQ,),q,
                             answers,weights)
        Q.DumpGift()
    else  :
        print()
        print('//// Skipped an incompatible combination ////') 
        print()
        print()

