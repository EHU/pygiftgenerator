# MP_SRelativity_CMCollision_NQ.py
# 
# Relativistic collisions between particles and particle and photons
# from the center of mass reference frame using pygiftgenerator.py
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# The copyright of this software corresponds to its authors, 
# Jon Saenz, 
# Idoia G. Gurtubay, 
# Zunbeltz Izaola, 
# employees at the University of the Basque Country, 
# Universidad del Pais Vasco/Euskal Herriko Unibertsitatea (UPV/EHU).
#
# Code released in 2020 under GPL 3.
# For details, see https://www.gnu.org/licenses/gpl.html
# and https://www.gnu.org/licenses/gpl.txt
# 
import math,sys,random

# Import the constructor of questions
from pygiftgenerator import GIFTNumQ

# Calculate the velocity of the CM, use named parameter to set the output file
class GET_CMv(GIFTNumQ):
    def __init__(self,rootCat,qName,question,units,tol,E,E0,ofile):
        GIFTNumQ.__init__(self,rootCat,qName,question,units,tol,ofileObject=ofile)
        self.E=E
        self.E0=E0
        self.c=299792458.
        self.v=self.c*math.sqrt((self.E-self.E0)/(self.E+self.E0))
    def Correct(self):
        return self.v

# Calculate the energy of the system measured from the CM
class GET_CME(GIFTNumQ):
    def __init__(self,rootCat,qName,question,units,tol,E,E0,ofile):
        GIFTNumQ.__init__(self,rootCat,qName,question,units,tol,ofileObject=ofile)
        self.E=E
        self.E0=E0
        self.Estar=math.sqrt(2*self.E0*(self.E+self.E0))
    def Correct(self):
        return self.Estar

# Usually, you call this 
# random.seed()
# But we need a fixed seed for repeatibility (regression tests)
random.seed(123897977)

# Speed of light
c=299792458.
one_eV_in_J=1.6021766208e-19 # J
# Parameters are built from these lists
ms={"proton":1.6726e-27,"electron":9.1094e-31,"neutron":1.6749e-27} # in kg
particles=list(ms.keys())
particles.sort()

nQuestions=5
category="pyGIFT/ModernPhysics/SpRel/CollisionsCMRefSystem/NQ"
tol=0.075
try:
    giftFileName=sys.argv[1]
except:
    N=len(sys.argv[0])
    giftFileName=sys.argv[0][:(N-2)]+"gift"
ofile=open(giftFileName,"w")
iq=0
for p in particles:
    if p=="electron":
       art="n"
    else:
       art=""
    # Mass in kg
    m=ms[p]
    # Energy at rest
    E0=m*c*c
    # Convert to MeV
    E0MeV=E0/1.6e-19/1.0e6
    for jq in range(nQuestions):
        # Randomly retrieve a value of gamma from a gaussian distribution
        Gamma=random.gauss(3,.5)
        EMeV=E0MeV*Gamma
        E=E0*Gamma
        question="[html]A %s %s with energy %.5g MeV impacts a second identical particle that is at rest. The energy of the %s at rest is E<sub>0</sub>\=%.3g MeV. Calculate the velocity of the center of mass of the system composed by both particles"%(art,p,EMeV,p,E0MeV)
        qName="SR_CMv_%3.3d"%(iq,)
        q=GET_CMv(category,qName,question,"m/s",tol,E,E0,ofile)
        q.DumpGift()
        question="[html]A %s %s with energy %.5g MeV impacts a second identical particle that is at rest. The energy of the %s at rest is E<sub>0</sub>\=%.3g MeV. Calculate the energy of the system measured from the center of mass of the system composed by both particles"%(art,p,EMeV,p,E0MeV)
        qName="SR_CME_%3.3d"%(iq,)
        q=GET_CME(category,qName,question,"MeV",tol,EMeV,E0MeV,ofile)
        q.DumpGift()
        iq=iq+1
ofile.close()



