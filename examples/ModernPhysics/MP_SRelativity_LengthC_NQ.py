# MP_SRelativity_LengthC_NQ.py
# 
# Application of pygiftgenerator to problems within the realm of
# length contraction
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# The copyright of this software corresponds to its authors, 
# Jon Saenz, 
# Idoia G. Gurtubay, 
# Zunbeltz Izaola, 
# employees at the University of the Basque Country, 
# Universidad del Pais Vasco/Euskal Herriko Unibertsitatea (UPV/EHU).
#
# Code released in 2020 under GPL 3.
# For details, see https://www.gnu.org/licenses/gpl.html
# and https://www.gnu.org/licenses/gpl.txt
# 
import numpy as np
import math
from pygiftgenerator import BuildNumQuestion

# Lorentz's factor
def Gamma(beta):
    return 1./math.sqrt(1-beta*beta)

# The path of the particle in S if we know beta in S and tau in S'
def PathInS(beta,taudot):
    c=299792458.0
    tau=taudot*Gamma(beta)
    Ls=tau*beta*c
    return Ls

# Get contracted length
def LengthContracted(beta,L):
    Ldot=L/Gamma(beta)
    return Ldot

# Velocities of the moving reference frame (as beta)
betas=np.array([ 2./5.,    3./4.,    4./7.,    5./6.,
                 1./2.,    2./3.,    1./3.,    4./5.,
                 6./7.,    9./10.,    10./11., 12./13. ])

# Disintegration times of particles (assuming they are not moving)
# Lengths of material objects
Ls={
    "A train":100,
    "A ruler":30.0e-2,
    "The Eiffel tower":324.,
    "A Skycraper in Dubai":330
}

# Keys in lengths
lkeys=Ls.keys()

for i in range(len(betas)):
    beta=betas[i]
    # Length contraction problems
    j=0
    for lkey in lkeys:
        L=Ls[lkey]
        rootCategory="pyGIFT/ModernPhysics/SpRel/LengthContraction/NQ"
        qname="LC_%3.3d"%(i*len(lkeys)+j,)
        qtext="[html]%s is %.4g m long as measured from reference frame S. How long is it, as measured by an observer moving with speed %.4gc in S?"%(lkey,L,beta)
        retValue=LengthContracted(beta,L)
        # This time, use the F77 paradigm (function instead of object and method)
        BuildNumQuestion(rootCategory,qname,qtext,"m",retValue,.1)
        j+=1
