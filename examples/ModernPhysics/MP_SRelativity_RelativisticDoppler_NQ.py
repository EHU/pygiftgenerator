# MP_SRelativity_RelativisticDoppler_NQ.py
# 
# Calculations related to Relativistic Doppler effect
# using pygiftgenerator.py
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# The copyright of this software corresponds to its authors, 
# Jon Saenz, 
# Idoia G. Gurtubay, 
# Zunbeltz Izaola, 
# employees at the University of the Basque Country, 
# Universidad del Pais Vasco/Euskal Herriko Unibertsitatea (UPV/EHU).
#
# Code released in 2020 under GPL 3.
# For details, see https://www.gnu.org/licenses/gpl.html
# and https://www.gnu.org/licenses/gpl.txt
# 
import math,sys,random

# Import the constructor of questions
from pygiftgenerator import GIFTNumQ

# Calculate the frequency of the radar signal after composing velocities
class GET_Doppler_Composed(GIFTNumQ):
    def __init__(self,rootCat,qName,question,units,tol,v1,v2,nu0,ofile):
        GIFTNumQ.__init__(self,rootCat,qName,question,units,tol,ofileObject=ofile)
        self.c=299792458.
        self.v1=v1*self.c
        self.v2=v2*self.c
        self.v=(self.v2-self.v1)/(1.-self.v1*self.v2/self.c/self.c)
        self.nu=nu0*math.sqrt((self.c-self.v)/(self.c+self.v))
    def Correct(self):
        # In MHz
        return self.nu/1.0e6

# Usually, you call this 
# random.seed()
# But we need a fixed seed for repeatibility (regression tests)
random.seed(123897977)

nQuestions=50
category="pyGIFT/ModernPhysics/SpRel/RelativisticDopplerEffect/NQ"
tol=0.075
try:
    giftFileName=sys.argv[1]
except:
    N=len(sys.argv[0])
    giftFileName=sys.argv[0][:(N-2)]+"gift"
ofile=open(giftFileName,"w")
tq=0
nQuestions=25
usedVs=[]
while (tq < nQuestions ):
    # Random velocities
    a=random.randint(1,7)
    b=random.randint(1,7)
    c=random.randint(8,20)
    if (a,b,c) in usedVs:
        tq+=1
        continue
    else:
        usedVs.append((a,b,c))
        v1=float(a)/float(c)
        v2=float(b)/float(c)
    # Randomly retrieve a value of frequency from a gaussian distribution
    nu0=random.gauss(6,1)*1.0e9 # in GHz
    question="A spaceship (A) travels with a speed %.4fc in the positive direction of the X axis and is equipped with a radar that emits electromagnetic radiation of frequency %.5g Hz. A second spaceship (B) travels with speed %.4fc, also in the direction of the positive X axis. Which is the frequency of the radar signal that is measured in spaceship B"%(v1,nu0,v2)
    qName="SR_Doppler_%3.3d"%(tq,)
    q=GET_Doppler_Composed(category,qName,question,"MHz",tol,v1,v2,nu0,ofile)
    q.DumpGift()
    tq+=1
ofile.close()


