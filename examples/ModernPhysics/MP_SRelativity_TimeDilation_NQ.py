# MP_SRelativity_TimeDilation_NQ.py
# 
# Generating multiple questions on time dilation and events
# considering Lorentz transformations using pygiftgenerator.py
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# The copyright of this software corresponds to its authors, 
# Jon Saenz, 
# Idoia G. Gurtubay, 
# Zunbeltz Izaola, 
# employees at the University of the Basque Country, 
# Universidad del Pais Vasco/Euskal Herriko Unibertsitatea (UPV/EHU).
#
# Code released in 2020 under GPL 3.
# For details, see https://www.gnu.org/licenses/gpl.html
# and https://www.gnu.org/licenses/gpl.txt
# 
import numpy as np
import math
from pygiftgenerator import BuildNumQuestion

# Lorentz's factor
def Gamma(beta):
    return 1./math.sqrt(1-beta*beta)

# The path of the particle in S if we know beta in S and tau in S'
def PathInS(beta,taudot):
    c=299792458.0
    tau=taudot*Gamma(beta)
    Ls=tau*beta*c
    return Ls

# Velocities of the moving reference frame (as beta)
betas=np.array([ 2./5.,    3./4.,    
                 1./2.,    2./3.,    1./3.,
               ])

# Disintegration times of particles (assuming they are not moving)
taus={
    "charged kaon":1.24e-8, 
    "charged pion":2.6e-8,
    "muon":2.197e-6,
    "meson":2.6e-8,
    "pion":8.4e-17
}

# Keys in times
tkeys=taus.keys()

for i in range(len(betas)):
    beta=betas[i]
    # Time dilation problems
    j=0
    for tkey in tkeys:
        taudot=taus[tkey]
        rootCategory="pyGIFT/ModernPhysics/SpRel/TimeDilation/NQ"
        qname="TD_%3.3d"%(i*len(tkeys)+j,)
        qtext="[html]A %s is travelling at speed %.4gc as measured from reference frame S. If the particle's disintegration time at rest is %.4g &mu;s, how long is its path, as measured in S?"%(tkey,beta,taudot*1.0e6)
        retValue=PathInS(beta,taudot)
        # This time, use the F77 paradigm (function instead of object and method)
        BuildNumQuestion(rootCategory,qname,qtext,"m",retValue,.1)
        j+=1


