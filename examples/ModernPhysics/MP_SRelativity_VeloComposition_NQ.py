# MP_SRelativity_VeloComposition_NQ.py
# 
# Preparing numerical questions regarding relativistic composition of
# velocities using pygiftgenerator.py
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# The copyright of this software corresponds to its authors, 
# Jon Saenz, 
# Idoia G. Gurtubay, 
# Zunbeltz Izaola, 
# employees at the University of the Basque Country, 
# Universidad del Pais Vasco/Euskal Herriko Unibertsitatea (UPV/EHU).
#
# Code released in 2020 under GPL 3.
# For details, see https://www.gnu.org/licenses/gpl.html
# and https://www.gnu.org/licenses/gpl.txt
# 

import numpy as np
from pygiftgenerator import BuildNumQuestion

betaa=np.array([
    2./5.,    3./4.,    
    1./2.,    2./3.,    1./3.,    
    ])

betab=-betaa

# Get velocity in S' when inputs are expressed as beta
def dxdt_dot(dxdt,v):
    return (dxdt-v)/(1.-dxdt*v)

rootCategory="pyGIFT/ModernPhysics/SpRel/CompositionOfVelocities/NQ"
for i in range(len(betaa)):
    for j in range(len(betab)):
        qname="SR_VC_%3.3d"%(i*len(betab)+j,)
        if i==j:
            continue
        qtext="Particles A and B are travelling at speeds %.4gc and %.4gc as measured from reference frame S. Which is B particle's speed as measured by an observer on A?"%(betaa[i],betab[j])
        retValue=dxdt_dot(betab[i],betaa[j])
	# This time, use the F77 paradigm (function instead of object and method)
        BuildNumQuestion(rootCategory,qname,qtext,"c",retValue,.1)
