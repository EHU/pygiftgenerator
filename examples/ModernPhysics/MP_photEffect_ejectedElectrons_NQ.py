# MP_photEffect_ejectedElectrons_NQ.py
#
# pygiftgenerator.py
# 
# A generator of numerical and multiple choice Moodle-based questions
# with the aim of covering large areas of physics teaching
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# The copyright of this software corresponds to its authors, 
# Jon Saenz, 
# Idoia G. Gurtubay, 
# Zunbeltz Izaola, 
# employees at the University of the Basque Country, 
# Universidad del Pais Vasco/Euskal Herriko Unibertsitatea (UPV/EHU).
#
# Code released in 2020 under GPL 3.
# For details, see https://www.gnu.org/licenses/gpl.html
# and https://www.gnu.org/licenses/gpl.txt
# 

import math
import random

# Import the constructor of questions
from pygiftgenerator import GIFTNumQ
# And the randomizers for parameter inputs
from pygiftgenerator import RandomInputParameters

# Find the maximum velocity
class GET_VMAX(GIFTNumQ):
    # Provide a constructor with the needed data (the ones that are passed
    # to the default constructor and teh new ones needed in this case
    def __init__(self,rootCat,qName,question,units,tol,W0,lambda1):
        GIFTNumQ.__init__(self,rootCat,qName,question,units,tol)
        self.W0=W0
        self.lambda1=lambda1
        self.h_Js=6.62606*pow(10,-34)
        self.c=3.0*pow(10,8)
        self.nm2m=1.0*pow(10,-9)
        self.eV2J=1.6021*pow(10,-19)
        self.m_e=9.109*pow(10,-31)
        self.ekmax=self.h_Js*self.c/(self.lambda1*self.nm2m) - self.W0*self.eV2J
        self.vmax = math.sqrt(2*self.ekmax/self.m_e)
    # This method calculates the solution and is the expected one
    # that needs being provided by this intance so that the
    # inherited class works
    def Correct(self):
        return self.vmax

# Find the De Broglie wavelenght of the electrons
class GET_LAMBDA_dBROGLIE(GIFTNumQ):
    # Provide a constructor with the needed data (the ones that are passed
    # to the default constructor and teh new ones needed in this case
    def __init__(self,rootCat,qName,question,units,tol,W0,lambda1):
        GIFTNumQ.__init__(self,rootCat,qName,question,units,tol)
        self.W0=W0
        self.lambda1=lambda1
        self.h_Js=6.62606*pow(10,-34)
        self.c=3.0*pow(10,8)
        self.nm2m=1.0*pow(10,-9)
        self.eV2J=1.6021*pow(10,-19)
        self.m_e=9.109*pow(10,-31)
        self.ekmax=self.h_Js*self.c/(self.lambda1*self.nm2m) - self.W0*self.eV2J
        self.vmax = math.sqrt(2*self.ekmax/self.m_e)
        self.lamb_dBroglie= self.h_Js/(self.m_e*self.vmax)/self.nm2m
    # This method calculates the solution and is the expected one
    # that needs being provided by this intance so that the
    # inherited class works
    def Correct(self):
        return self.lamb_dBroglie


# Usually, you call this 
# random.seed()
# But we need a fixed seed for repeatibility (regression tests)
random.seed(123897977)


# ---- All other questions with more variables

# Parameters are built from these lists
W0_list=[ 1.92, 2.09,2.24,2.35,2.46,2.48,2.70,2.74]
lambda1_list=[250, 275, 300, 325, 350, 400, 424, 450]

#Pushing the parameters to the orginal case
#W0_list=[2.70]
#lambda1_list=[450]

# Random parameters 
randIntParams=RandomInputParameters()
randIntParams.AppendParameters(W0_list)
randIntParams.AppendParameters(lambda1_list)
####

nQuestions=24
# Same tolerance for all questions ! CHECK!
tol=0.075
for iq in range(nQuestions):
    plist=randIntParams.GetParameterList()
    if len(plist)==0:
        print("// No questions left after ",iq)
        break
    W0=plist[0]
    lambda1=plist[1]
    # Do not build the questions for vmx and lamda at the same time with the
    # same parameters
    if 0<=iq<12:
       category="pyGIFT/ModernPhysics/QuMech/PhotoelectricEffect/EjectedElectronsProperties/NQ"
       question="The photoelectric work function of a certain metal is W<sub>0</sub>=%.2f eV. If light having a wavelength of %.0f nm falls on the surface, find the maximum velocity of the photoelectrons emitted from this surface "%(W0, lambda1)
       qName="Get_vmax_%3.3d"%(iq,)
       q=GET_VMAX(category,qName,question,"m/s",tol,W0,lambda1)
    else :
       category="pyGIFT/ModernPhysics/QuMech/PhotoelectricEffect/EjectedElectronsProperties/NQ"
       question="The photoelectric work function of a certain metal is W<sub>0</sub>=%.2f eV. If light having a wavelength of %.0f nm falls on the surface, find the de Broglie wavelength of the photoelectrons emitted from this surface "%(W0, lambda1)
       qName="Get_lambda_deBroglie_%3.3d"%(iq,)
       q=GET_LAMBDA_dBROGLIE(category,qName,question,"nm",tol,W0,lambda1)

       
    q.DumpGift()


    
