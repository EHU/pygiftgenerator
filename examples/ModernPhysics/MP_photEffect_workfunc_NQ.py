# MP_photEffect_workfunc_NQ.py
#
# pygiftgenerator.py
# 
# A generator of numerical and multiple choice Moodle-based questions
# with the aim of covering large areas of physics teaching
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# The copyright of this software corresponds to its authors, 
# Jon Saenz, 
# Idoia G. Gurtubay, 
# Zunbeltz Izaola, 
# employees at the University of the Basque Country, 
# Universidad del Pais Vasco/Euskal Herriko Unibertsitatea (UPV/EHU).
#
# Code released in 2020 under GPL 3.
# For details, see https://www.gnu.org/licenses/gpl.html
# and https://www.gnu.org/licenses/gpl.txt
# 

import math
import random

# Import the constructor of questions
from pygiftgenerator import GIFTNumQ
# And the randomizers for parameter inputs
from pygiftgenerator import RandomInputParameters

# Find the threshold lambda
class GET_LAMBDA0(GIFTNumQ):
    # Provide a constructor with the needed data (the ones that are passed
    # to the default constructor and teh new ones needed in this case
    def __init__(self,rootCat,qName,question,units,tol,W0):
        GIFTNumQ.__init__(self,rootCat,qName,question,units,tol)
        self.W0=W0
        self.h_eVs=4.1356*pow(10,-15)
        self.c=3*pow(10,8)
        self.nm2m=1.0*pow(10,-9)
        self.f0=self.W0/self.h_eVs
        self.lambda0=self.c/self.f0/self.nm2m
    # This method calculates the solution and is the expected one
    # that needs being provided by this intance so that the
    # inherited class works
    def Correct(self):
        return self.lambda0


# Usually, you call this 
# random.seed()
# But we need a fixed seed for repeatibility (regression tests)
random.seed(123897977)



# --- In the first question there is only one variable ---------
    
# Parameters are built from these lists
W0_list=[ 1.92, 2.09,2.24,2.35,2.46,2.70, 4.20,3.68, 5.11, 4.53 ]

#Pushing the parameters to the orginal case
#W0_list=[2.70]

# Random parameters 
randIntParams=RandomInputParameters()
randIntParams.AppendParameters(W0_list)
####

nQuestions=10
# Same tolerance for all questions 
tol=0.025
for iq in range(nQuestions):
    plist=randIntParams.GetParameterList()
    if len(plist)==0:
        print("// No questions left after ",iq)
        break
    W0=plist[0]
    category="pyGIFT/ModernPhysics/QuMech/PhotoelectricEffect/WorkFunction/NQ"
    question="The photoelectric work function of a certain metal is W<sub>0</sub>=%.2f eV. If light having a wavelength of 300 nm falls on the surface, find the photoelectric threshold wavelength for this surface "%(W0)
    qName="Get_lambda0_%3.3d"%(iq,)
    q=GET_LAMBDA0(category,qName,question,"nm",tol,W0)

       
    q.DumpGift()


    
