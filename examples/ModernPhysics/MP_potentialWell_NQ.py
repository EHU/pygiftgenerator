# MP_potentialWell_NQ.py
#
# pygiftgenerator.py
# 
# A generator of numerical and multiple choice Moodle-based questions
# with the aim of covering large areas of physics teaching
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# The copyright of this software corresponds to its authors, 
# Jon Saenz, 
# Idoia G. Gurtubay, 
# Zunbeltz Izaola, 
# employees at the University of the Basque Country, 
# Universidad del Pais Vasco/Euskal Herriko Unibertsitatea (UPV/EHU).
#
# Code released in 2020 under GPL 3.
# For details, see https://www.gnu.org/licenses/gpl.html
# and https://www.gnu.org/licenses/gpl.txt
# 

#
import math
import random

# Import the constructor of questions
from pygiftgenerator import GIFTNumQ
# And the randomizers for parameter inputs
from pygiftgenerator import RandomInputParameters

# Find the Energy for each level
class GET_En(GIFTNumQ):
    # Provide a constructor with the needed data (the ones that are passed
    # to the default constructor and teh new ones needed in this case
    def __init__(self,rootCat,qName,question,units,tol,Lnm,n):
        GIFTNumQ.__init__(self,rootCat,qName,question,units,tol)
        self.Lnm=Lnm
        self.n=n
        self.h_Js=6.62606*pow(10,-34)
        self.nm2m=1.0*pow(10,-9)
        self.m_e=9.109*pow(10,-31)
        self.eV2J=1.6021*pow(10,-19)
        self.Lm=self.Lnm*self.nm2m
        self.EnJ=pow(self.h_Js,2)/(8*self.m_e*pow(self.Lm,2))*pow(n,2)
        self.EneV=self.EnJ/self.eV2J
    # This method calculates the solution and is the expected one
    # that needs being provided by this intance so that the
    # inherited class works
    def Correct(self):
        return self.EneV


# Find the probablity of finding the particle
class GET_PROB(GIFTNumQ):
    # Provide a constructor with the needed data (the ones that are passed
    # to the default constructor and teh new ones needed in this case
    def __init__(self,rootCat,qName,question,units,tol,Lnm,n,B):
        GIFTNumQ.__init__(self,rootCat,qName,question,units,tol)
        self.Lnm=Lnm
        self.n=n
        self.B=B
        self.pi=math.acos(-1.0)
        self.h_Js=6.62606*pow(10,-34)
        self.nm2m=1.0*pow(10,-9)
        self.m_e=9.109*pow(10,-31)
        self.eV2J=1.6021*pow(10,-19)
        self.Lm=self.Lnm*self.nm2m
        self.A=self.B*self.Lm
        #self.prob=self.A/self.Lm-1.0/(2.0*self.n*self.pi)*math.sin(2.0*n*self.pi*self.A/self.Lm) 
        self.prob=self.A/self.Lm-1.0/(2.0*self.n*self.pi)*math.sin(2.0*n*self.pi*self.A/self.Lm) *100
    # This method calculates the solution and is the expected one
    # that needs being provided by this intance so that the
    # inherited class works
    def Correct(self):
        return self.prob

# Find the expectation value of x
class GET_EXPVAL(GIFTNumQ):
    # Provide a constructor with the needed data (the ones that are passed
    # to the default constructor and teh new ones needed in this case
    def __init__(self,rootCat,qName,question,units,tol,Lnm,n):
        GIFTNumQ.__init__(self,rootCat,qName,question,units,tol)
        self.Lnm=Lnm
        self.n=n
        self.expval=self.Lnm/2.0
    # This method calculates the solution and is the expected one
    # that needs being provided by this intance so that the
    # inherited class works
    def Correct(self):
        return self.expval


# Usually, you call this 
# random.seed()
# But we need a fixed seed for repeatibility (regression tests)
random.seed(123897977)

 
# Parameters are built from these lists
Lnm_list=[0.1,0.11,0.12,0.13,0.14,0.15,0.16,0.17,0.18,0.19,0.2,0.21,0.22,0.23,0.24,0.25,0.26,0.27,0.28,0.29,0.3]
n_list=[1,2,3,4]
B_list=[0.25,0.5,0.75]

# Random parameters 
randIntParams=RandomInputParameters()
randIntParams.AppendParameters(Lnm_list)
randIntParams.AppendParameters(n_list)
randIntParams.AppendParameters(B_list)
####

nQuestions=21
# Same tolerance for all questions ! CHECK!
tol=0.075
for iq in range(nQuestions):
    plist=randIntParams.GetParameterList()
    if len(plist)==0:
        print("// No questions left after ",iq)
        break
    Lnm=plist[0]
    n=plist[1]
    B=plist[2]
    # Do not build the questions for I2 and I3 at the same time with the
    # same parameters
    if 0<=iq<7:
       category="pyGIFT/ModernPhysics/QuMech/PotentialWell/NQ"
       question="An electron is in a one-dimensional box of length L=%.2f nm. Find the energy of the state n=%.0f in eV.  "%(Lnm, n)
       qName="Get_En_%3.3d"%(iq,)
       q=GET_En(category,qName,question,"eV",tol,Lnm,n)
    elif 7<=iq<14:
       category="pyGIFT/ModernPhysics/QuMech/PotentialWell/NQ"
       question="An electron is in a one-dimensional box of length L=%.2f nm. Find the probability of finding the particle between 0 and %.2fL  for the state n=%.0f.  "%(Lnm,B,n)
       qName="Get_probability_%3.3d"%(iq,)
       q=GET_PROB(category,qName,question,"%",tol,Lnm,n,B)
    else:
       category="pyGIFT/ModernPhysics/QuMech/PotentialWell/NQ"
       question="An electron is in a one-dimensional box of length L=%.2f nm. Find the expectation value of the position x, for the state n=%.0f.  "%(Lnm,n)
       qName="Get_expectvalue_x_%3.3d"%(iq,)
       q=GET_EXPVAL(category,qName,question,"nm",tol,Lnm,n)


       
    q.DumpGift()


    
