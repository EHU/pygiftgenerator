# TH_Blackbody_Planck_NQ.py
# 
# An application of the Blackbody function to estimate intensity
# from blackbodies (Planck law). Numerical Questions.
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# The copyright of this software corresponds to its authors, 
# Jon Saenz, 
# Idoia G. Gurtubay, 
# Zunbeltz Izaola, 
# employees at the University of the Basque Country, 
# Universidad del Pais Vasco/Euskal Herriko Unibertsitatea (UPV/EHU).
#
# Code released in 2020 under GPL 3.
# For details, see https://www.gnu.org/licenses/gpl.html
# and https://www.gnu.org/licenses/gpl.txt
# 
import math
# Import the Fortran-like constructor of questions
from pygiftgenerator import GIFTNumQ
import random
import numpy

# Blackbody - intensity as a function of T and wavelength, in K and m,
# respectively, following Liou, Atmospheric Radiation book, eq. 1.2.4
class BlackBody(GIFTNumQ):
    def __init__(self,qName,T,wl):
        # Category
        category="pyGIFT/Thermodynamics/BlackBodyRadiationPlanck/NQ"
        # Units, common for all cases
        units="W m<sup>-2</sup> sr<sup>-1</sup> &mu;m<sup>-1</sup>"
        # store input parameters
        self.t=T
        self.wl=wl
        # Question text in the constructor
        question="[html]Which is the intensity radiated by a blackbody at temperature %10.4f K at &lambda;\=%12.5e &mu;m?"%(T,wl/1.0e-6)
        GIFTNumQ.__init__(self,category,qName,question,units,0.1)
    # Intensity as the correct answer
    def Correct( self ):
        h=6.62607004e-34
        c=299792458.
        K=1.3806e-23
        C1=2.*h*c*c
        c2=h*c/K
        lm5=math.pow(self.wl,-5.)
        theexp=c2/self.wl/self.t
        return C1*lm5/(math.exp(theexp)-1.)


# initialize RNG
# In general
# random.seed()
# For regression tests must keep this fixed
random.seed(123897977)

# Number of questions around every value
nQs=2

# question ID
qID=0

# Very high temperatures
tH=numpy.array([5000.,4000.])
# Radiation in the hundred-nanometer scale
wLS=numpy.array([500.,700.])*1.0e-9
for t in tH:
    for w in wLS:
        for iq in range(nQs):
            qName="blackbody_%4.4d"%(qID,)
            temp=random.gauss(t,100.)
            wl=random.gauss(w/1.0e-6,0.005)*1.0e-6
            q=BlackBody(qName,temp,wl)
            q.DumpGift()
            qID+=1

# Low temperatures
tL=numpy.array([300.,280.,275.])
# Radiation in the micrometer scale
wLL=numpy.array([10.,15.,20.])*1.0e-6
for t in tL:
    for w in wLL:
        for iq in range(nQs):
            qName="blackbody_%4.4d"%(qID,)
            temp=random.gauss(t,10.)
            wl=random.gauss(w/1.0e-6,5)*1.0e-6
            q=BlackBody(qName,temp,wl)
            q.DumpGift()
            qID+=1



