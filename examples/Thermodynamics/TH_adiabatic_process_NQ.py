# TH_adiabatic_process_NQ.py
# 
# Solution of problems related to adiabatic evolutions
# in the atmosphere. Numerical Questions.
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# The copyright of this software corresponds to its authors, 
# Jon Saenz, 
# Idoia G. Gurtubay, 
# Zunbeltz Izaola, 
# employees at the University of the Basque Country, 
# Universidad del Pais Vasco/Euskal Herriko Unibertsitatea (UPV/EHU).
#
# Code released in 2020 under GPL 3.
# For details, see https://www.gnu.org/licenses/gpl.html
# and https://www.gnu.org/licenses/gpl.txt
# 

# We need the mathematical functions to calculate
import math

# We randomly select from input parameters in a list of values
import random


# Import the constructor of questions
from pygiftgenerator import GIFTNumQ
from pygiftgenerator import RandomInputParameters

# Calculate potential temperature
class PotentialTemperature(GIFTNumQ):
    def __init__(self,rootCat,qName,question,P,Tk,tol):
        GIFTNumQ.__init__(self,rootCat,qName,question,"K",tol)    
        self.P=P
        self.Tk=Tk
        # Constants needed by this subclass
        self.Rd=287.
        self.cp=1004.
        self.P0=100000.
    def Correct(self):
        theta=self.Tk*math.pow(self.P0/self.P,self.Rd/self.cp)
        return theta

# This should be
# random.seed()
# But for regression tests we use a fixed seed
random.seed(123897977)

# General properties of all the constructors
# Tolerance, 3%
tol=0.03
# Number of questions from each type
nQuestions=10

catString="pyGIFT/Thermodynamics/AdiabaticProcesses/NQ"
# This time, we use a list of different Random Input Selectors, because
# it would not be very wise to tell the students that
# T at 500 hPa is 25 degrees C
Ps=[500,550]
Ts=[-30,-28,-25,-20]
# New input parameters
R1=RandomInputParameters()
R1.AppendParameters(Ps)
R1.AppendParameters(Ts)
########################
Ps=[600]
Ts=[-25,-22,-20,-17,-15,-12]
# New input parameters
R2=RandomInputParameters()
R2.AppendParameters(Ps)
R2.AppendParameters(Ts)
Ps=[700]
Ts=[-15,-12,-10,-8,-5]
# New input parameters
R3=RandomInputParameters()
R3.AppendParameters(Ps)
R3.AppendParameters(Ts)
Ps=[850]
Ts=[-10,-7,-5,0,3,5]
# New input parameters
R4=RandomInputParameters()
R4.AppendParameters(Ps)
R4.AppendParameters(Ts)
# This is now a set of more or less consistent randomizers that
# will produce reasonable sets of temperatures and pressures
randomizers=[R1,R2,R3,R4]

qFmt="Compute the potential temperature of an air parcel at pressure %6.1f mb and %4.1f degrees Celsius"
for iQ in range(nQuestions):
    # Select either R1 ... R4 (typical 500 hPa to 850 hPa)
    Ri=randomizers[random.randint(0,3)]
    plist=Ri.GetParameterList()
    if len(plist)==0:
        sys.stderr.write("Unable to print the requested number of questions\n")
    else:
        P=plist[0]
        T=plist[1]
        q=PotentialTemperature(catString,"PT%3.3d"%(iQ,),qFmt%(P,T),
                        P*100,T+273.15,tol)
        q.DumpGift()


