# TH_equation_state_NQ.py
# 
# Application of pygiftgenerator.py to the solution of Numerical Questions
# dealing with the equation of state of dry and moist air
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# The copyright of this software corresponds to its authors, 
# Jon Saenz, 
# Idoia G. Gurtubay, 
# Zunbeltz Izaola, 
# employees at the University of the Basque Country, 
# Universidad del Pais Vasco/Euskal Herriko Unibertsitatea (UPV/EHU).
#
# Code released in 2020 under GPL 3.
# For details, see https://www.gnu.org/licenses/gpl.html
# and https://www.gnu.org/licenses/gpl.txt
# 

# We need the mathematical functions to calculate
import math

# We randomly select from input parameters in a list of values
import random


# Import the constructor of questions
from pygiftgenerator import GIFTNumQ
from pygiftgenerator import RandomInputParameters


# First example, build an object that calculates the density of dry air
class DryDensity(GIFTNumQ):
    # Provide a constructor with the needed data (the ones that are passed
    # to the default constructor and teh new ones needed in this case
    # pressure and temperature in Kelvin)
    def __init__(self,rootCat,qName,question,units,tol,P,Tk):
        GIFTNumQ.__init__(self,rootCat,qName,question,units,tol)
        self.P=P
        self.Tk=Tk
        # Constant for dry air
        self.Rd=287.
    # This method calculates the solution and is the expected one
    # that needs being provided by this intance so that the
    # inherited class works
    def Correct(self):
        density=self.P/self.Rd/self.Tk
        return density

# Second example, density of moist air computed from partial pressure
# of water vapour
class MoistDensityE(GIFTNumQ):
    def __init__(self,rootCat,qName,question,P,Tk,eh,units,tol):
        GIFTNumQ.__init__(self,rootCat,qName,question,units,tol)
        self.P=P
        self.Tk=Tk
        # Constant for dry air
        self.Rd=287.
        # Constant for water vapour
        self.Rv=461.
        self.eh=eh
    # Compute the density of dry air and add the density of water vapour
    def Correct(self):
        density=(self.P-self.eh)/self.Rd/self.Tk+self.eh/self.Rv/self.Tk
        return density

# Compute virtual temperature of moist air
class VirtualTemperature(GIFTNumQ):
    def __init__(self,rootCat,qName,question,P,Tk,eh,units,tol):
        GIFTNumQ.__init__(self,rootCat,qName,question,units,tol)
        self.P=P
        self.Tk=Tk
        self.Rd=287.
        self.Rv=461.
        self.epsilon=self.Rd/self.Rv
        self.eh=eh
    def Correct(self):
        Tv=self.Tk/(1.-(self.eh/self.P)*(1.-self.epsilon))
        return Tv

# Usually, you call this 
# random.seed()
# But we need a fixed seed for repeatibility (regression tests)
random.seed(123897977)

# General properties of all the constructors
# Tolerance, 3%
tol=0.03
# Number of questions from each type
nQuestions=10

# These will be the input parameters for all the cases
# Pressure in millibars (converted later)
Ps=[1000,1005,995, 985,990,1010,1020]
# Pressure in Celsius (converted to K after the enunciate is ready)
Ts=[ -5,  -1, 0 ,  5,  10, 20, 25,  30, 35 ]
# Partial pressures of water vapour in hPa
es=[  5 , 10 , 20 , 30 , 50 , 100 , 200 ]


# This category
catString="pyGIFT/Thermodynamics/EquationOfState/NQ"
# Build a selectr of input parameters for dry air
randIntParams=RandomInputParameters()
randIntParams.AppendParameters(Ps)
randIntParams.AppendParameters(Ts)
# Text of the question
qFmt="Compute the density of dry air at pressure %6.1f mb and %4.1f degrees Celsius"
for iQ in range(nQuestions):
    plist=randIntParams.GetParameterList()
    if len(plist)==0:
        sys.stderr.write("Unable to print the requested number of questions\n")
    else:
        P=plist[0]
        T=plist[1]
        q=DryDensity(catString,"DD%3.3d"%(iQ,),qFmt%(P,T),
                     "kg m<sup>-3</sup>",tol,P*100,T+273.15)
        q.DumpGift()

# Produce values of moist density
# First, update the table of parameters, it can grow dynamically
# It will be reinitialized later
randIntParams.AppendParameters(es)
# Text for all these questions
qFmt="Compute the density of moist air at pressure %6.1f mb and %4.1f degrees Celsius if partial pressure of water vapour is %3.1f mb"
for iQ in range(nQuestions):
    plist=randIntParams.GetParameterList()
    if len(plist)==0:
        sys.stderr.write("Unable to print the requested number of questions\n")
    else:
        P=plist[0]
        T=plist[1]
        eh=plist[2]
        q=MoistDensityE(catString,"MD%3.3d"%(iQ,),qFmt%(P,T,eh),
                        P*100,T+273.15,eh*100.,"kg m<sup>-3</sup>",tol)
        q.DumpGift()

qFmt="Compute the virtual temperature of moist air at pressure %6.1f mb and %4.1f degrees Celsius if partial pressure of water vapour is %3.1f mb"
for iQ in range(nQuestions):
    plist=randIntParams.GetParameterList()
    if len(plist)==0:
        sys.stderr.write("Unable to print the requested number of questions\n")
    else:
        P=plist[0]
        T=plist[1]
        eh=plist[2]
        q=VirtualTemperature(catString,"VT%3.3d"%(iQ,),qFmt%(P,T,eh),
                        P*100,T+273.15,eh*100.,"K",tol)
        q.DumpGift()

