# TH_relative_humidity_NQ.py
# 
# Thermodynamics of the atmosphere. Application of pygiftgenerator.py
# to the computation of relative humidity from other moidture indices.
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# The copyright of this software corresponds to its authors, 
# Jon Saenz, 
# Idoia G. Gurtubay, 
# Zunbeltz Izaola, 
# employees at the University of the Basque Country, 
# Universidad del Pais Vasco/Euskal Herriko Unibertsitatea (UPV/EHU).
#
# Code released in 2020 under GPL 3.
# For details, see https://www.gnu.org/licenses/gpl.html
# and https://www.gnu.org/licenses/gpl.txt
# 

# We need the mathematical functions to calculate
import math

# We randomly select from input parameters in a list of values
import random


# Import the constructor of questions
from pygiftgenerator import GIFTNumQ
from pygiftgenerator import RandomInputParameters


# Apply the Clausius Clapeyron equation (one of the approximations)
# To find the saturation pressure of water vapour    
class ClausiusClapeyron(GIFTNumQ):
    def __init__(self,rootCat,qName,question,Tk,units,tol):
        GIFTNumQ.__init__(self,rootCat,qName,question,units,tol)
        self.Tk=Tk
        self.L=2.5e6
        self.Rv=461.
    def Correct(self):
        es=611.*math.exp(self.L/self.Rv*(1./273.-1./self.Tk))
        return es

# Compute relative humidity
class RelativeHumidity(GIFTNumQ):
    def __init__(self,rootCat,qName,question,P,Tk,w,units,tol):
        GIFTNumQ.__init__(self,rootCat,qName,question,units,tol)
        self.P=P
        self.Tk=Tk
        self.Rd=287.
        self.Rv=461.
        self.epsilon=self.Rd/self.Rv
        self.w=w
        self.L=2.5e6
    # This method computes the water vapour pressure from mixing ratio
    def Ev(self):
        ev=self.w*self.P/(self.w+self.epsilon)
        return ev
    # This one computes the saturation water vapour pressure
    def Es(self):
        es=611.*math.exp(self.L/self.Rv*(1./273.-1./self.Tk))
        return es
    # This method uses information from other methods to get relative humidity
    def RH(self):
        ev=self.Ev()
        es=self.Es()
        return 100*ev/es*((self.P-es)/(self.P-ev))
    # This one calls the previous method, the class knows how to do it
    def Correct(self):
        rh=self.RH()
        return rh


# Usually, you call this 
# random.seed()
# But we need a fixed seed for repeatibility (regression tests)
random.seed(123897977)

# General properties of all the constructors
# Tolerance, 3%
tol=0.03
# Number of questions from each type
nQuestions=10
# This category
catString="pyGIFT/Thermodynamics/RelativeHumidity/NQ"

Ps=[995.,1000.,1004.,1012.,1015.]
Ts=[5. , 10. , 15.  ,20.  ,25.   ,30.]
ws=[0.5 , 1.  , 2.  , 4.  , 6. , 9.,  12.]

# New input parameters
randIntParams=RandomInputParameters()
randIntParams.AppendParameters(Ps)
randIntParams.AppendParameters(Ts)
randIntParams.AppendParameters(ws)

qFmt="Compute the relative humidity of moist air at pressure %6.1f mb and %4.1f degrees Celsius if mixing ratio of water vapour is %3.1f gr/kg"
for iQ in range(nQuestions):
    plist=randIntParams.GetParameterList()
    if len(plist)==0:
        sys.stderr.write("Unable to print the requested number of questions\n")
    else:
        P=plist[0]
        T=plist[1]
        w=plist[2]
        q=RelativeHumidity(catString,"RH%3.3d"%(iQ,),qFmt%(P,T,w),
                        P*100,T+273.15,w/1000.,"%",tol)
        q.DumpGift()


