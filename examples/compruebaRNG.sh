FILES="./Thermodynamics/TH_relative_humidity_NQ.py ./Thermodynamics/TH_equation_state_NQ.py ./ElectroMagnetism/EM_Gaussian_Surface_NQ.py ./ElectroMagnetism/EM_RC_circuit_NQ.py ./ElectroMagnetism/EM_DC_circuit_MC.py ./ElectroMagnetism/EM_Magnetic_Field_NQ.py ./ElectroMagnetism/EM_Magnetic_Field_MC.py ./ElectroMagnetism/EM_DC_circuit_NQ.py ./Mechanics/ME_coupled_pulleys_MC.py ./Mechanics/ME_plane_motor_NQ.py ./Mechanics/ME_plane_system_MC.py ./Mechanics/ME_coupled_pulleys_NQ.py ./ModernPhysics/MP_potentialWell_NQ.py ./ModernPhysics/MP_photEffect_ejectedElectrons_NQ.py ./ModernPhysics/MP_photEffect_workfunc_NQ.py ./Thermodynamics/TH_adiabatic_process_NQ.py ./ModernPhysics/MP_SRelativity_RelativisticDoppler_NQ.py ./Thermodynamics/TH_Blackbody_Planck_NQ.py ./ModernPhysics/MP_SRelativity_CMCollision_NQ.py"
for f in $FILES
do
	gvim $f 
done

