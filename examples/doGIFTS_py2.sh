find . -name "*.gift" -exec rm {} \;

DIRS="ElectroMagnetism Mechanics ModernPhysics Thermodynamics"
theWD=$( pwd )
for D in $DIRS
do
    echo "Directory: $D"
    cd $theWD/$D
for f in *.py
do
    target=$( echo -n $f | sed -e 's/\.py/\.gift/g' )
    REDIRECT=YES
    # These ones generate the GIFT output internally by using ofile
    # argument
    case $f
    in
	MP_SRelativity_RelativisticDoppl* )
	    REDIRECT=NO
	;;
	MP_SRelativity_CM* )
	    REDIRECT=NO
	;;
        ME_binary* )
	    REDIRECT=NO
	;;
    esac
    # echo $REDIRECT
    if [ $REDIRECT == YES ]
    then
	echo "Processing $f with redirection"
	python2 $f > $target
    else
	echo "Processing $f without redirection"
	python2 $f $target
    fi
done
cd ${theWD}
done


