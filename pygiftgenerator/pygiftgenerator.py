# pygiftgenerator.py
# 
# A generator of numerical Moodle-based questions
# with the aim of covering large areas of physics teaching
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# The copyright of this software corresponds to its authors, 
# Jon Saenz, 
# Idoia G. Gurtubay, 
# Zunbeltz Izaola, 
# employees at the University of the Basque Country, 
# Universidad del Pais Vasco, Euskal Herriko Unibertsitatea UPV/EHU.
#
# pygiftgenerator code released in 2020 under GPL 3.
# For details, see https://www.gnu.org/licenses/gpl.html
# and https://www.gnu.org/licenses/gpl.txt
# 

import random
import sys

# Object designed to get random combinations of input parameters
# You don't really want the answers to be repeated,
# so, get random combination of parameters if and only if they have not been
# used in a previous question
class RandomInputParameters:
    """ 

    RandomInputParameters produces sets of parameters randomly taken
    from input lists to this kind of objects. The output parameters
    keep the same order in the output tuple as the one used for
    the lists when added to the object during its creation.

    """
    def __init__(self):
        """Creation of a RandomInputParameters instance."""
        self.myLists={}
        self.ParamID=0
        self.UsedCombinations=[]
        self.isInitialized=False
    # Must be appended in the order they will be processed
    def AppendParameters(self,newList):
        """ 

        Append a list to the set of lists from which parameters are taken.
        Must be called at least once.

        newList is the list which is being added to the set of lists stored
                in each instance.

        """
        self.ParamID=self.ParamID+1
        self.myLists[self.ParamID]=newList
        self.isInitialized=False
    # Only after all parameters have been appended
    def GetParameterList(self):
        """

        After a set of list has been created, this function returns
        (as a tuple) a set of elements taken from the set of lists
        in the same order as they have been introduced when the object
        was created by using AppendParameters().

        The returned elements are randomly taken from the set of lists.

        """
        # The first time, initialize some tables for faster execution
        if not self.isInitialized:
            self.isInitialized=True
            self.keys=list(self.myLists.keys())
            self.keys.sort()
            self.lengths={}
            for k in self.keys:
                # -1 because of the use of randitnt() later
                self.lengths[k]=len(self.myLists[k])-1
        # In any case, try a combination
        self.tries=1000
        trynumber=0
        thisTryParams=[]
        while trynumber<self.tries:
            for k in self.keys:
                ili=random.randint(0,self.lengths[k])
                thisTryParams.append(self.myLists[k][ili])
            if thisTryParams in  self.UsedCombinations:
                trynumber+=1
                # Restart
                thisTryParams=[]
                continue
            else:
                self.UsedCombinations.append(thisTryParams)
                # exit
                break
        return thisTryParams
            
        
# For object-oriented people the best option is to just use instances
# inherited from this object
class GIFTNumQ:
    """GIFTNumQ is the class used to produce numerical questions."""
    def __init__(self,rootCategory,qName,question,units,tolerance=0.05,
                 resFormat="%.4g",errFormat="%.4g",ofileObject=sys.stdout):
        """

        Constructor for numerical question instances:

        rootCategory : The category of the Moodle question bank where this
                       question is arranged.

        qName : The name of the question to be shown in Moodle.

        question : The text of the question as seen by students.

        units : The string used for units in the answer.

        tolerance : The relative error taken as a valid answer (default 0.05).

        resFormat : The format used for the answers in the GIFT file 
                    (default %.4g).

        errFormat : The format used for the error in the GIFT file 
                    (default %.4g).

        ofileObject : The stream used for the output (sys.stdout by default).

        """
        self.category=rootCategory
        self.name=qName
        self.question=question
        self.units=units
        self.tolerance=tolerance
        self.resFormat=resFormat
        self.errFormat=errFormat
        self.ofileObject=ofileObject
    def SetValue(self,retValue):
        """ 

        Manually store the correct answer for this kind of question. It
        is usually not needed to call this, since an inheritad class
        can just override the default behaviour by defining the
        method Correct(), which is the preferred option.

        """
        self.retValue=retValue
    def DumpGift(self):
        """Dump this instance as a GIFT string to the output stream."""
        try:
            self.retValue=self.Correct()
        except:
            # Assuming the user has already set the return value
            pass 
        errorEst=self.retValue*self.tolerance
        strFmt="#%s:%s"%(self.resFormat,self.errFormat)
        strAns=strFmt%(self.retValue,errorEst)
        self.ofileObject.write("/"*20+"\n")
        self.ofileObject.write("$CATEGORY: "+self.category+"\n\n")
        ostr="::"+self.name+"::"+self.question
        ostr+=" {"+strAns+"} %s\n\n"%(self.units,)
        self.ofileObject.write(ostr)

# A function-like API for Fortran programmers ;-)
def BuildNumQuestion(rootCategory,qName,question,units,retValue,tolerance=0.05,
                 resFormat="%.4g",errFormat="%.4g",ofileObject=sys.stdout):
    """

    BuildNumQuestion is a function which can be called to produce
    numerical questions without using the class-like interface for users
    not well versed in object-oriented terminology.

    The input parameters for this function are:

        rootCategory : The category of the Moodle question bank where this
                       question is arranged.

        qName : The name of the question to be shown in Moodle.

        question : The text of the question as seen by students.

        units : The string used for units in the answer.

        retValue : The value which must be written as correct by students.

        tolerance : The relative error taken as a valid answer (default 0.05).

        resFormat : The format used for the answers in the GIFT file 
                    (default %.4g).

        errFormat : The format used for the error in the GIFT file 
                    (default %.4g).

        ofileObject :  The stream used for the output (sys.stdout by default).

    """
    q=GIFTNumQ(rootCategory,qName,question,units,tolerance,
                 resFormat,errFormat,ofileObject)
    q.SetValue(retValue)
    q.DumpGift()
    del q

# For object-oriented people the best option is to just use instances
# inherited from this object for multiple-choice questions
class GIFTMulChoiceQ:
    """Class designed to produce numerical multiple choice questions."""
    def __init__(self,rootCategory,qName,question,answers,weights=None,
                 skipRepeated=True,ofileObject=sys.stdout):
        """

        Constructor for numerical multiple-choice questions.

        rootCategory : The category in which the question is classified.

        qName : The name of the question after it is imported into Moodle.

        question : The text of the question as seen by students.

        answers : A list with the potential answers.

        weights=None : The weights (%) applied to the questions. By default,
                       None is provided and, in this case, a list with
                       the same number of answers is created, assigning 100 (%)
                       to the first answer and 0 to the others. 
                       If the instructor wants to pass
                       four answers, with the third one getting 100%
                       of the score, the first one -5% and the other
                       ones (third, fourth) -10% he/she should
                       pass [-5,100,-10,-10] in this argument.

        skipRepeated=True : If there are repeated values in the list of asnwers
                       they are reported as comments in the output
                       GIFT file, so that no questions is produced.

        ofileObject=sys.stdout : Stream where the question is written to
                                 (default: sys.stdout).

        """
        self.category=rootCategory
        self.name=qName
        self.question=question
        self.answers=answers
        self.N=len(answers)
        if weights is None:
            self.weights=[100]
            for i in range(1,self.N):
                self.weights.append(0)
        else:
            self.weights=weights
        self.isUnique=self.CheckRepeatedAnswers()
        self.ofileObject=ofileObject
    def CheckRepeatedAnswers(self):
        isUnique=True
        olist=[self.answers[0]]
        for i in range(1,self.N):
            a=self.answers[i]
            if a in olist:
                isUnique=False
                break
            olist.append(a)
        return isUnique
    def outString(self,i):
        thisWeight=self.weights[i]
        if thisWeight==100:
                outstr="=%s\n"%(self.answers[i],)
        elif thisWeight==0:
            outstr="~%s\n"%(self.answers[i],)
        else:
            outstr="~%%%d%%%s\n"%(self.weights[i],self.answers[i])
        return outstr
    def DumpGift(self):
        """ Dump the question object as a GIFT stream 

        :returns: Logical value stating whether que question was dumped or
              not due to any repetition of wrong answers

        """
        self.ofileObject.write("/"*20+"\n")
        if self.isUnique:
            startChar=""
        else:
            startChar="// REP_ANS "
        self.ofileObject.write(startChar+"$CATEGORY:"+self.category+"\n\n")
        self.ofileObject.write(startChar+"::"+self.name+"::"+
                               self.question+" {\n")
        for i in range(self.N):
            outstr=self.outString(i)
            self.ofileObject.write(startChar+outstr)  
        self.ofileObject.write(startChar+"}\n")
        self.ofileObject.write("\n")
        return self.isUnique
            
                


# A function-like API for Fortran programmers ;-)
def BuildMulChoiceQuestion(categ,qName,qText,answ,wei=None,
                           skipRepeated=True,ofiOb=sys.stdout):
    """ 

    BuildMulChoiceQuestion is a function to build questions
    without using the class-like interface.

    categ : Category into which the question is imported.

    qName : Name of the question after it is imported by Moodle.

    qText : Text of the questions as seen by students.

    answ : List of potential answers.

    wei=None : Weights used to grade the answer (-100..100) converted to %.
               If None is passed [100,0,0...0] is built with the same
               number of elements as potential answers. Thus, the first
               answer is assumed as correct and given 100% weight.

    skipRepeated=True : If there are repeated values in the list of asnwers
                       they are reported as comments in the output
                       GIFT file, so that no questions is produced.

    ofiOb=sys.stdout : Stream where the question is written to 
                       (default sys.stdout).

    :returns: Logical value stating whether que question was dumped or
              not due to any repetition of wrong answers

    """
    q=GIFTMulChoiceQ(categ,qName,qText,answ,wei,skipRepeated,ofiOb)
    q.DumpGift()
    isUnique=q.isUnique
    del q
    return isUnique

